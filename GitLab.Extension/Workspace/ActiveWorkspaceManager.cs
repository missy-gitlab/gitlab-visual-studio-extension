﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using GitLab.Extension.Utility.Results;
using GitLab.Extension.Utility.Results.Errors;

namespace GitLab.Extension.Workspace
{
    public class ActiveWorkspaceManager : IActiveWorkspaceStateObservable, IDisposable
    {
        private readonly BehaviorSubject<WorkspaceState> _workspaceStateSubject =
            new BehaviorSubject<WorkspaceState>(WorkspaceState.NoActiveWorkspace);
        
        private readonly IDisposable _workspaceStateObservableConnection;
        public WorkspaceState Current => _workspaceStateSubject.Value;
        
        public ActiveWorkspaceManager(
            IActiveWorkspaceIdObservable activeWorkspaceIdObservable,
            IWorkspaceFactory workspaceFactory)
        {
            _workspaceStateObservableConnection =
                activeWorkspaceIdObservable
                    .DistinctUntilChanged()
                    .Do(_ => DisposeActiveWorkspace())
                    .SelectMany(
                        workspaceId =>
                        {
                            if (!workspaceId.HasValue)
                            {
                                return Observable.Return(NoActiveWorkspace.Instance);
                            }

                            return Observable
                                .FromAsync(() => CreateWorkspaceAsync(workspaceFactory, workspaceId.Value))
                                .StartWith(WorkspaceState.WorkspaceInitializing(workspaceId.Value));
                        })
                    .Subscribe(_workspaceStateSubject);
        }
        
        private void DisposeActiveWorkspace()
        {
            if (_workspaceStateSubject.Value is WorkspaceActive active)
            {
                active.Workspace.Dispose();
            }
        }

        public void Dispose()
        {
            DisposeActiveWorkspace();
            _workspaceStateObservableConnection?.Dispose();
        }

        public IDisposable Subscribe(
            IObserver<WorkspaceState> observer)
        {
            return _workspaceStateSubject.Subscribe(observer);
        }

        private static async Task<WorkspaceState> CreateWorkspaceAsync(
            IWorkspaceFactory workspaceFactory,
            WorkspaceId workspaceId)
        {
            try
            {
                return await workspaceFactory
                    .CreateWorkspaceAsync(workspaceId)
                    .Match<IWorkspace, WorkspaceState>(
                        WorkspaceState.WorkspaceActive,
                        error => WorkspaceState.WorkspaceInitializationFailed(workspaceId, error))
                    .ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                return WorkspaceState.WorkspaceInitializationFailed(
                    workspaceId,
                    new ExceptionalError(ex));
            }
        }
    }
}
