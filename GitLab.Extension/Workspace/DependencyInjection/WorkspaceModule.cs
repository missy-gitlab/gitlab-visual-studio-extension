﻿using System;
using Autofac;

namespace GitLab.Extension.Workspace.DependencyInjection
{
    public class WorkspaceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            // Register WorkspaceManager as a singleton
            builder
                .RegisterType<ActiveWorkspaceManager>()
                .As<IActiveWorkspaceStateObservable>()
                .SingleInstance();

            builder
                .RegisterType<ActiveWorkspaceIdService>()
                .As<IWorkspaceEventHandler>()
                .As<IActiveWorkspaceIdObservable>()
                .SingleInstance();
            
            // Register WorkspaceFactory
            builder
                .RegisterType<WorkspaceFactory>()
                .As<IWorkspaceFactory>();
        }
    }
}
