using GitLab.Extension.LanguageServer;

namespace GitLab.Extension.Workspace
{
    public class LsClientProvider : IWorkspaceLsClientProvider
    {
        private readonly ILsClientManager _clientManager;

        public LsClientProvider(
            ILsClientManager clientManager)
        {
            _clientManager = clientManager;
        }

        public ILsClient GetClient(
            WorkspaceId workspaceId)
        {
            return _clientManager.GetClient(
                workspaceId.SolutionName,
                workspaceId.SolutionPath);
        }
    }
}
