using System;
using System.IO;
using System.Threading.Tasks;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace GitLab.Extension.Workspace
{
    public static class WorkspaceUtilities
    {
        /// <summary> Asynchronously retrieves the current workspace identifier. </summary>
        /// <returns>A task that asynchronously returns the current Workspace identifier </returns>
        /// <exception cref="InvalidOperationException">Thrown if no active solution is found.</exception>
        public static async Task<WorkspaceId> GetCurrentWorkspaceIdAsync()
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
            return GetCurrentWorkspaceId();
        }

        /// <summary> Retrieves the current workspace identifier. </summary>
        /// <returns> The current Workspace identifier </returns>
        /// <exception type="COMException">Thrown with RPC_E_WRONG_THREAD when called on any thread other than the main UI thread.</exception>
        /// <exception cref="InvalidOperationException">Thrown if no active solution is found.</exception>
        public static WorkspaceId GetCurrentWorkspaceId()
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            var dte2 = (DTE2)Package.GetGlobalService(typeof(SDTE));
            var workspaceId = GetWorkspaceIdViaSolution(dte2.Solution);

            return workspaceId;
        }

        private static WorkspaceId GetWorkspaceIdViaSolution(
            _Solution solution)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            if (solution == null)
            {
                throw new InvalidOperationException("No active solution.");
            }

            var solutionName = Path.GetFileNameWithoutExtension(solution.FullName);
            var solutionPath = Path.GetDirectoryName(solution.FileName);

            return new WorkspaceId(solutionName, solutionPath);
        }
    }
}
