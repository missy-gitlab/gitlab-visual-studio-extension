using System;
using System.Threading.Tasks;
using GitLab.Extension.Utility.Results;
using GitLab.Extension.Workspace.Model;

namespace GitLab.Extension.Workspace
{
    public class WorkspaceFactory : IWorkspaceFactory
    {
        public Task<Result<IWorkspace>> CreateWorkspaceAsync(
            WorkspaceId id)
        {
            try
            {
                IWorkspace workspace = new WorkspaceModel(id);
                return Task.FromResult(Result.Ok(workspace));
            }
            catch (Exception ex)
            {
                return Task.FromResult(Result.Error<IWorkspace>(ex));
            }
        }
    }
}
