using System;
using System.Reactive.Subjects;
using Serilog;

namespace GitLab.Extension.Workspace
{
    public class ActiveWorkspaceIdService : IWorkspaceEventHandler, IActiveWorkspaceIdObservable
    {
        private readonly ILogger _logger;
        
        private readonly BehaviorSubject<WorkspaceId?> _activeWorkspaceIdSubject
            = new BehaviorSubject<WorkspaceId?>(null);

        public ActiveWorkspaceIdService(
            ILogger logger)
        {
            _logger = logger;
        }

        public void OnWorkspaceOpen(WorkspaceId workspaceId)
        {
            if (_activeWorkspaceIdSubject.Value == workspaceId)
            {
                _logger.Verbose(
                    "Attempted to open workspace with id `{workspaceId}`, but it's already the active workspace. No action taken.",
                    workspaceId);
                
                return;
            }
            
            _logger.Information("Opening workspace with id `{workspaceId}`.", workspaceId);
            _activeWorkspaceIdSubject.OnNext(workspaceId);
        }

        public void OnWorkspaceClose(WorkspaceId workspaceId)
        {
            if (_activeWorkspaceIdSubject.Value != workspaceId)
            {
                _logger.Verbose(
                    "Attempted to close workspace with id `{workspaceId}`, but it's not the current active workspace. No action taken.",
                    workspaceId);
                
                return;
            }
            
            _logger.Information(
                "Closing the active workspace with id `{workspaceId}`.",
                workspaceId);
            
            _activeWorkspaceIdSubject.OnNext(null);
        }

        public IDisposable Subscribe(
            IObserver<WorkspaceId?> observer)
        {
            return _activeWorkspaceIdSubject.Subscribe(observer);
        }
    }
}
