using System.Threading.Tasks;
using GitLab.Extension.Utility.Results;

namespace GitLab.Extension.Workspace
{
    public interface IWorkspaceFactory
    {
        Task<Result<IWorkspace>> CreateWorkspaceAsync(
            WorkspaceId id);
    }
}
