namespace GitLab.Extension.Workspace.Model
{
    public class WorkspaceModel : IWorkspace
    {
        public WorkspaceModel(WorkspaceId id)
        {
            Id = id;
        }

        public WorkspaceId Id { get; }

        public void Dispose()
        {}
    }
}
