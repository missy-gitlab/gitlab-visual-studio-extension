﻿using Serilog;
using System.Security.Cryptography;
using System.Text;

namespace GitLab.Extension.SettingsUtil
{
    public class ProtectImpl : ISettingsProtect
    {
        private readonly ILogger _logger;
        
        public ProtectImpl(ILogger logger)
        {
            _logger = logger;
        }

        public string Protect(string data)
        {
            try
            {
                var dataAsBytes = UTF8Encoding.UTF8.GetBytes(data);
                var protectedAsBytes = ProtectedData.Protect(dataAsBytes, null, DataProtectionScope.CurrentUser);
                return System.Convert.ToBase64String(protectedAsBytes);
            }
            catch (CryptographicException e)
            {
                _logger.Debug(e, $"{nameof(Protect)} Protect exception");
                return string.Empty;
            }
        }

        public string Unprotect(string protectedData)
        {
            try
            {
                var protectedDataAsBytes = System.Convert.FromBase64String(protectedData);
                var dataAsBytes = ProtectedData.Unprotect(protectedDataAsBytes, null, DataProtectionScope.CurrentUser);
                return UTF8Encoding.UTF8.GetString(dataAsBytes);
            }
            catch (CryptographicException e)
            {
                _logger.Debug(e, $"{nameof(Unprotect)} exception");
                return string.Empty;
            }
        }
    }
}
