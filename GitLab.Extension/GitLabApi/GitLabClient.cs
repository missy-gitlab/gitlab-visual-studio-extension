﻿using System.Net;
using System.Net.Http;
using System.Text.Json.Nodes;
using System.Threading;
using System.Threading.Tasks;
using GitLab.Extension.GitLabApi.Extensions;
using GitLab.Extension.GitLabApi.Model;
using GitLab.Extension.Utility.Results;

namespace GitLab.Extension.GitLabApi
{
    public class GitLabClient : IGitLabClient
    {
        private readonly HttpClient _httpClient;

        public GitLabClient(
            HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public bool IsSelfHosted => !Constants.KnownGitLabUris.Contains(_httpClient.BaseAddress);

        public async Task<Result<Metadata>> GetMetadataAsync(
            CancellationToken cancellationToken = default)
        {
            const string requestUri = "api/v4/metadata";

            return await _httpClient.SendAsync<Metadata>(
                requestUri,
                HttpMethod.Get,
                cancellationToken: cancellationToken);
        }

        public async Task<Result<bool>> IsCodeSuggestionsEnabledForProjectAsync(
            CodeSuggestionsEnabledRequest requestModel,
            CancellationToken cancellationToken = default)
        {
            const string requestUri = "api/v4/code_suggestions/enabled";

            var responseResult =
                await _httpClient.SendAsJsonAsync(
                    requestUri,
                    HttpMethod.Post,
                    requestModel,
                    cancellationToken);

            return responseResult.Map(x => x.StatusCode == HttpStatusCode.OK);
        }

        public async Task<Result<bool>> IsCodeSuggestionsEnabledForUserAsync(
            CancellationToken cancellationToken = default)
        {
            const string query = @"
            query aiAccess {
              currentUser {
                duoCodeSuggestionsAvailable
              }
            }";

            var responseObjectResult = await _httpClient.SendGraphQlQueryAsync<JsonObject>(query, cancellationToken);

            return responseObjectResult
                .Map(responseObject =>
                    responseObject["data"]?["currentUser"]?["duoCodeSuggestionsAvailable"]
                        ?.GetValue<bool>() ?? false);
        }
    }
}