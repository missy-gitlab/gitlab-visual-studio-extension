﻿using Microsoft.Extensions.DependencyInjection;

namespace GitLab.Extension.GitLabApi.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddGitLabHttpClient(
            this IServiceCollection serviceCollection)
        {
            serviceCollection.AddHttpClient(Constants.HttpClientName);

            return serviceCollection
                .AddSingleton<IGitLabClientFactory, GitLabClientFactory>();
        }
    }
}