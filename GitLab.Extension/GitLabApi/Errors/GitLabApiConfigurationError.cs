﻿using System.Collections.Generic;
using GitLab.Extension.Utility.Results;
using GitLab.Extension.Utility.Results.Errors;

namespace GitLab.Extension.GitLabApi.Errors
{
    public class GitLabApiConfigurationError : Error
    {
        public GitLabApiConfigurationError(
            IEnumerable<IError> errors
        ) : base(reasons: errors)
        {}
    }

    public class InvalidBaseUrl : Error
    {
        public InvalidBaseUrl(
            string message
        ) : base(message)
        {}
    }

    public class InvalidAccessToken : Error
    {
        public InvalidAccessToken(
            string message
        ) : base(message)
        {}
    }
}