﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using GitLab.Extension.GitLabApi.Serialization;

namespace GitLab.Extension.GitLabApi
{
    internal static class Constants
    {
        public static string HttpClientName = "gitlab";

        public static readonly List<Uri> KnownGitLabUris =
            new List<Uri>
            {
                new Uri("https://gitlab.com"),
                new Uri("https://www.gitlab.com")
            };

        public static readonly JsonSerializerOptions GitLabJsonSerializerOptions =
            new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                Converters = { new GitLabVersionJsonConverter() }
            };
    }
}