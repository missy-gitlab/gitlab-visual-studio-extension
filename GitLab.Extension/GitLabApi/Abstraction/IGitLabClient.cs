﻿using System.Threading;
using System.Threading.Tasks;
using GitLab.Extension.GitLabApi.Model;
using GitLab.Extension.Utility.Results;

namespace GitLab.Extension.GitLabApi
{
    public interface IGitLabClient
    {
        bool IsSelfHosted { get; }

        Task<Result<Metadata>> GetMetadataAsync(
            CancellationToken cancellationToken = default);

        Task<Result<bool>> IsCodeSuggestionsEnabledForProjectAsync(
            CodeSuggestionsEnabledRequest request,
            CancellationToken cancellationToken = default);

        Task<Result<bool>> IsCodeSuggestionsEnabledForUserAsync(
            CancellationToken cancellationToken = default);
    }
}