﻿namespace GitLab.Extension.GitLabApi
{
    public interface IGitLabClientFactory
    {
        IGitLabClient CreateClient(
            GitLabApiSettings settings);
    }
}