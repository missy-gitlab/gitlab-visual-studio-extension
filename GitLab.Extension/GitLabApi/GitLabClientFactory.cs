﻿using System.Net.Http;
using System.Net.Http.Headers;

namespace GitLab.Extension.GitLabApi
{
    public class GitLabClientFactory : IGitLabClientFactory
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public GitLabClientFactory(
            IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IGitLabClient CreateClient(
            GitLabApiSettings settings)
        {
            var httpClient = _httpClientFactory.CreateClient(Constants.HttpClientName);
            ConfigureHttpClient(httpClient, settings);

            var client = new GitLabClient(httpClient);
            
            return client;
        }
        
        private static void ConfigureHttpClient(
            HttpClient client,
            GitLabApiSettings settings)
        {
            client.BaseAddress = settings.BaseUri;
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("bearer", settings.AccessToken);
        }
    }
}