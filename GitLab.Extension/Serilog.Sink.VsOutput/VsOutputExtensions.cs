﻿using Serilog;
using Serilog.Configuration;
using System;
using System.Windows;

namespace GitLab.Extension.Serilog.Sink.VsOutput
{
    public static class VsOutputExtensions
    {
        public static LoggerConfiguration VsOutput(
            this LoggerSinkConfiguration loggerConfiguration,
            UIElement uiElement,
            IFormatProvider formatProvider = null)
        {
            return loggerConfiguration.Sink(new VsOutput(formatProvider, uiElement));
        }
    }
}
