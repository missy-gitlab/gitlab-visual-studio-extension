﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLab.Extension.Status
{
    public delegate void MenuItemClickHandler(ExtensionStatusControl control);

    public interface IStatusControl
    {
        MenuItemClickHandler MenuItemClickHandler { get; set; }

        void StatusEnabled();

        void StatusDisabled();

        void StatusDisabled(string message);

        void StatusError(string message);

        void StatusLoading();
    }
}
