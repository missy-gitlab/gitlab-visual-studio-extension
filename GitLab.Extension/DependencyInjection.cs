﻿using System;
using System.Reactive.Subjects;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutofacSerilogIntegration;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.CodeSuggestions.State;
using GitLab.Extension.CodeSuggestions.State.DependencyInjection;
using GitLab.Extension.Command;
using GitLab.Extension.GitLabApi.DependencyInjection;
using GitLab.Extension.InfoBar;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.LanguageServer.Models;
using GitLab.Extension.SettingsUtil;
using GitLab.Extension.SettingsUtil.Observable;
using GitLab.Extension.Status;
using GitLab.Extension.Utility.Git.DependencyInjection;
using GitLab.Extension.VS;
using GitLab.Extension.Workspace;
using GitLab.Extension.Workspace.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.ComponentModelHost;
using Microsoft.VisualStudio.Language.Suggestions;

namespace GitLab.Extension
{
    public class DependencyInjection
    {
        private static DependencyInjection _instance;
        private static ISettings _settings;

        private DependencyInjection()
        {
            var container = RegisterComponents();
            Scope = container.BeginLifetimeScope();

            _settings = Scope.Resolve<ISettings>();
        }

        public static DependencyInjection Instance => _instance ?? (_instance = new DependencyInjection());

        public ILifetimeScope Scope { get; }

        private IContainer RegisterComponents()
        {
            var builder = new ContainerBuilder();

            builder.Populate(
                new ServiceCollection().AddGitLabHttpClient());

            builder.AddGitClient();

            // Commands
            builder.RegisterCommands();

            // CodeSuggestions

            builder.RegisterType<GitlabProposalSource>();
            builder.RegisterType<GitlabProposalManager>();
            builder
                .RegisterType<LanguageManager>()
                .As<ILanguageManager>()
                .SingleInstance();

            // builder
            //     .RegisterType<CodeSuggestionStateObservable>()
            //     .As<IObservable<CodeSuggestionState>>()
            //     .SingleInstance();

            builder.RegisterModule<CodeSuggestionsEnabledStateModule>();

            // CodeSuggestion - Telemetry

            builder
                .RegisterType<SuggestionServiceTelemetryAdaptor>()
                .SingleInstance()
                .AutoActivate();

            builder
                .RegisterType<GitLabCodeSuggestionTelemetryController>()
                .As<IGitLabCodeSuggestionTelemetryController>();

            builder.Register(c =>
            {
                var components = GlobalServiceProvider.GetService(typeof(SComponentModel)) as IComponentModel;
                return components?.GetService<SuggestionServiceBase>();
            });

            // Workspace
            builder.RegisterModule<WorkspaceModule>();
            
            builder
                .RegisterType<LsClientProvider>()
                .As<IWorkspaceLsClientProvider>();

            // LanguageServer

            builder
                .RegisterType<LsClientManager>()
                .As<ILsClientManager>()
                .SingleInstance();
            builder
                .RegisterType<LsClientTs>()
                .As<ILsClient>();
            builder
                .RegisterType<LsProcessManagerTs>()
                .As<ILsProcessManager>()
                .SingleInstance();
            builder.RegisterType<LsClientRpc>();

            // LanguageServerGlobalEvents

            builder
                .Register(c => new Subject<TokenValidationNotification>())
                .As<IObservable<TokenValidationNotification>>()
                .As<IObserver<TokenValidationNotification>>()
                .SingleInstance();

            // Settings

            builder
                .RegisterType<Settings>()
                .As<ISettings>()
                .SingleInstance();
            builder
                .RegisterType<RegistryStorage>()
                .As<ISettingsStorage>()
                .SingleInstance();
            builder
                .RegisterType<ProtectImpl>()
                .As<ISettingsProtect>()
                .SingleInstance();
            builder
                .RegisterType<SettingsObservable>()
                .As<IObservable<ISettings>>()
                .SingleInstance();

            // Status

            builder
                .RegisterType<StatusBar>()
                .SingleInstance();
            builder
                .RegisterType<ExtensionStatusControl>()
                .As<IStatusControl>()
                .SingleInstance();

            // Logging

            builder.RegisterLogger();

            // InfoBar
            builder
                .RegisterType<InfoBarFactory>()
                .As<IInfoBarFactory>()
                .SingleInstance();

            // Notification Service
            builder
                .RegisterType<InvalidTokenNotificationService>()
                .AutoActivate();

            return builder.Build();
        }
    }
}