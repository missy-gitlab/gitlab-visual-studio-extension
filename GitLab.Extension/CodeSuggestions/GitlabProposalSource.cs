﻿using GitLab.Extension.LanguageServer;
using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.TextManager.Interop;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using GitLab.Extension.CodeSuggestions.Model;
using GitLab.Extension.CodeSuggestions.State;
using Serilog;
using GitLab.Extension.LanguageServer.Models;
using GitLab.Extension.Workspace;

namespace GitLab.Extension.CodeSuggestions
{
    /// <summary>
    /// Generate inline proposals for a unique IWpfTextView instance.
    /// Each open file will have its own GitlabProposalSource instance.
    /// When the file is closed, the associated GitlabProposalSource instance will be disposed.
    /// </summary>
    public class GitlabProposalSource : ProposalSourceBase
    {
        public const string SourceName = nameof(GitlabProposalSource);
        private readonly IWpfTextView _textView;
        private readonly IWorkspaceLsClientProvider _lsClientProvider;
        private readonly Status.StatusBar _statusBar;
        private readonly ILogger _logger;

        private bool _disposed = false;
        private string _fullFilePath;
        private ILsClient _lsClient;
        private string _relativeFilePath;
        private WorkspaceId _workspaceId;
        
        private bool _isCodeSuggestionsEnabled;
        private readonly IDisposable _codeSuggestionStateSubscription;
        
        public GitlabProposalSource(
            IWorkspaceLsClientProvider lsClientProvider,
            Status.StatusBar statusBar,
            IWpfTextView textView,
            ICodeSuggestionsEnabledStateProvider codeSuggestionsEnabledStateProvider,
            ILogger logger)
        {
            _logger = logger;
            _lsClientProvider = lsClientProvider;
            
            _textView = textView;
            _statusBar = statusBar;

            _codeSuggestionStateSubscription =
                codeSuggestionsEnabledStateProvider
                    .IsCodeSuggestionsEnabledObservable
                    .Do(x => _isCodeSuggestionsEnabled = x)
                    .Subscribe();

            var uiElement = _textView as UIElement;

#pragma warning disable VSTHRD001 // Avoid legacy thread switching APIs
            uiElement.Dispatcher.Invoke(new Action(() =>
            {
                ThreadHelper.ThrowIfNotOnUIThread();

                _workspaceId = WorkspaceUtilities.GetCurrentWorkspaceId();
                

                _fullFilePath = GetTextViewFilePath();
                if (_fullFilePath.StartsWith(_workspaceId.SolutionPath))
                    _relativeFilePath = _fullFilePath.Substring(_workspaceId.SolutionPath.Length + 1);
                else
                    _relativeFilePath = _fullFilePath;
                
                _logger.Debug("Initialized GitlabProposalSource with Workspace ID {WorkspaceId} and File Path {FilePath}", _workspaceId, _fullFilePath);
            }));
#pragma warning restore VSTHRD001 // Avoid legacy thread switching APIs

            _textView.TextBuffer.Changed += TextBuffer_ChangedAsync;
            _textView.Closed += textView_Closed;
        }

        private void textView_Closed(object sender, EventArgs e)
        {
            _logger.Debug("Closing text buffer for {FilePath}.", _relativeFilePath);
            
            if (!_isCodeSuggestionsEnabled)
            {
                _logger.Debug("Code suggestions are disabled. Closing text view for {FilePath} is ignored.", _relativeFilePath);
                return;
            }

            if (_lsClient == null)
            {
                _logger.Debug("Language server client not initialized at the time of text view closure for {FilePath}.", _relativeFilePath);
                return;
            }

            _ = _lsClient.SendTextDocumentDidCloseAsync(_relativeFilePath);
            _logger.Debug("Closed document {FilePath} and notified language server.", _relativeFilePath);
        }

        public async Task<bool> StartLanguageServerClientAsync()
        {
            try
            {
                if (!_isCodeSuggestionsEnabled)
                {
                    _logger.Debug("Code suggestions are disabled. Not starting the language server client for {FilePath}.", _relativeFilePath);
                    return false;
                }

                if (_lsClient != null)
                    await _lsClient.DisposeAsync();
                
                _lsClient = _lsClientProvider.GetClient(_workspaceId);
                await _lsClient.ConnectAsync();
                await TextBuffer_OpenedAsync();

                _statusBar.CodeSuggestionsClearError();
                _logger.Debug("Ensured language server client is available for {FilePath}.", _relativeFilePath);
                
                return true;
            }
            catch(Exception ex)
            {
                _logger.Error(ex, "Exception occurred while starting the language server client for {FilePath}.", _relativeFilePath);
                _statusBar.CodeSuggestionsError("Exception occured starting the language server. If this error continues, please reach out to support.");
                return false;
            }
        }

        /// <summary>
        /// Get the filename we are editing
        /// </summary>
        /// <returns></returns>
        private string GetTextViewFilePath()
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            _textView.TextBuffer.Properties.TryGetProperty(typeof(IVsTextBuffer), out IVsTextBuffer bufferAdapter);

            var persistFileFormat = bufferAdapter as IPersistFileFormat;
            if (persistFileFormat == null)
            {
                _logger.Debug("Failed to obtain file path for text view. Buffer adapter does not support IPersistFileFormat.");
                return null;
            }

            persistFileFormat.GetCurFile(out var filepath, out _);
            _logger.Debug("Retrieved file path: {FilePath} for text view.", filepath);
            return string.IsNullOrEmpty(filepath) 
                ? null 
                : filepath;
        }

        private async Task TextBuffer_OpenedAsync()
        {
            _logger.Debug("Opening text buffer for {FilePath}.", _relativeFilePath);
            
            if (!_isCodeSuggestionsEnabled)
            {
                _logger.Debug("Code suggestions are disabled. Opening text view for {FilePath} is ignored.", _relativeFilePath);
                return;
            }

            try
            {
                var lsClient = await EnsureLanguageServerAsync();
                await lsClient.SendTextDocumentDidOpenAsync(
                    _relativeFilePath,
                    _textView.TextBuffer.CurrentSnapshot.Version.VersionNumber,
                    _textView.TextBuffer.CurrentSnapshot.GetText());
            
                _logger.Debug("Sent document open notification to language server for {FilePath}.", _relativeFilePath);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception occurred while sending open document notification to language server for {FilePath}.", _relativeFilePath);
            }
        }

#pragma warning disable VSTHRD100
#pragma warning disable VSTHRD200
        /// <summary>
        /// Send changes to language server.
        /// </summary>
        /// <remarks>
        /// This async method must return void to be a valid event handler.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void TextBuffer_ChangedAsync(object sender, TextContentChangedEventArgs e)
        {
            _logger.Debug("Text buffer changed for {FilePath}. Processing changes...", _relativeFilePath);
            
            try
            {
                _logger.Debug($"{nameof(TextBuffer_ChangedAsync)}");

                if (!_isCodeSuggestionsEnabled)
                {
                    _logger.Debug("Code suggestions are disabled. Ignoring changes for {FilePath}.", _relativeFilePath);
                    return;
                }

                var lsClient = await EnsureLanguageServerAsync();
                switch(lsClient.TextDocumentSyncKind)
                {
                    case TextDocumentSyncKind.Full:
                        await SendChangedFullAsync(sender, e);
                        return;
                    case TextDocumentSyncKind.Incremental:
                        await SendChangedIncrementalAsync(sender, e);
                        return;
                    case TextDocumentSyncKind.None:
                    default:
                        return;
                }
            }
            catch (Exception ex)
            {
                // Don't crash visual studio by handling all exceptions
                _logger.Error(ex, "Exception occurred while sending change document notification to language server for {FilePath}.", _relativeFilePath);
            }
        }

        /// <summary>
        /// Send changes to language server using the Incremental sync method
        /// </summary>
        /// <remarks>
        /// This async method must return void to be a valid event handler.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async Task SendChangedIncrementalAsync(object sender, TextContentChangedEventArgs e)
        {
            _logger.Debug("Starting to send incremental changes for {FilePath}.", _relativeFilePath);

            if (e.Changes.Count == 0)
            {
                _logger.Debug("No changes to send for {FilePath}.", _relativeFilePath);
                return;
            }
            
            try
            {
                var versionNumber = e.AfterVersion.VersionNumber;

                var changes = new TextDocumentContentChangeEvent[e.Changes.Count];

                for (int changeIndex = 0; changeIndex < e.Changes.Count; changeIndex++)
                {
                    var oldStartPosition = e.Changes[changeIndex].OldSpan.Start;
                    var oldEndPosition = e.Changes[changeIndex].OldSpan.End;
                    var changedText = e.Changes[changeIndex].NewText;

                    var start = PositionToLineAndCharacter(e.Before, oldStartPosition);
                    var end = PositionToLineAndCharacter(e.Before, oldEndPosition);

                    changes[changeIndex] = new TextDocumentContentChangeEvent
                    {
                        range = new Range
                        {
                            start = new Position
                            {
                                line = (uint)start.line,
                                character = (uint)start.character,
                            },
                            end = new Position
                            {
                                line = (uint)end.line,
                                character = (uint)end.character,
                            }
                        },
                        text = changedText,
                    };
                    
                    _logger.Debug("Recorded change for {FilePath}: Start {StartLine}:{StartChar} End {EndLine}:{EndChar} Text [{Text}].", 
                        _relativeFilePath, start.line, start.character, end.line, end.character, changedText);
                }

                await _lsClient.SendTextDocumentDidChangeAsync(
                    _relativeFilePath, versionNumber, changes);
                
                _logger.Debug("Incremental changes sent successfully for {FilePath}.", _relativeFilePath);
            }
            catch (Exception ex)
            {
                // Don't crash visual studio by handling all exceptions
                _logger.Error(ex, "Error sending incremental changes for {FilePath}.", _relativeFilePath);
            }
        }

        private (int line, int character) PositionToLineAndCharacter(ITextSnapshot snapshot, int position)
        {
            //if (position == 0)
            //    return (0, 0);

            var line = snapshot.GetLineFromPosition(position);
            var lineNumber = line.LineNumber;
            var character = position - line.Start;

            return (lineNumber, character);
        }

        /// <summary>
        /// Send changes to language server using the Full sync method
        /// </summary>
        /// <remarks>
        /// This async method must return void to be a valid event handler.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async Task SendChangedFullAsync(object sender, TextContentChangedEventArgs e)
        {
            _logger.Debug("Starting to send full text changes for {FilePath}.", _relativeFilePath);
            
            try
            {
                var fullText = e.After.GetText();
                var versionNumber = e.AfterVersion.VersionNumber;
                
                _logger.Debug("Sending full text (length {Length}) to language server for {FilePath} at version {Version}.", 
                    fullText.Length, _relativeFilePath, versionNumber);

                await _lsClient.SendTextDocumentDidChangeAsync(
                    _relativeFilePath,
                    versionNumber,
                    fullText);
                
                _logger.Debug("Full text changes sent successfully for {FilePath}.", _relativeFilePath);
            }
            catch (Exception ex)
            {
                // Don't crash visual studio by handling all exceptions
                _logger.Error(ex, "Error sending full text changes for {FilePath}.", _relativeFilePath);
            }
        }
#pragma warning restore VSTHRD100
#pragma warning restore VSTHRD200

        /// <summary>
        /// Called by Visual Studio to get a list of proposals from our extension. This
        /// triggers a call to get a code suggestion.
        /// </summary>
        /// <param name="caret"></param>
        /// <param name="completionState"></param>
        /// <param name="scenario"></param>
        /// <param name="triggeringCharacter"></param>
        /// <param name="token"></param>
        /// <returns>Returns a GitlabProposalCollection instance or null on error.</returns>
        public override async Task<ProposalCollectionBase> RequestProposalsAsync(
            VirtualSnapshotPoint caret, 
            CompletionState completionState, 
            ProposalScenario scenario, 
            char triggeringCharacter, 
            CancellationToken token)
        {
            _logger.Debug("Requesting proposals for {FilePath} at position {Position}. Scenario: {Scenario}, Triggering Character: '{TriggerChar}'", 
                _relativeFilePath, caret.Position, scenario.ToString(), triggeringCharacter.ToString());
            
            if (_disposed)
            {
                _logger.Warning("Attempt to request proposals after GitlabProposalSource has been disposed for {FilePath}.", _relativeFilePath);
                return null;
            }
                

            // Don't request a suggestion if code suggestions are disabled
            if (!_isCodeSuggestionsEnabled)
            {
                _logger.Debug("Code suggestions are disabled. No proposals will be requested for {FilePath}.", _relativeFilePath);
                return null;
            }

            var error = null as string;

            try
            {
                _statusBar.CodeSuggestionsInProgressStart();

                // If the extension was just configured, we will need
                // to start the language server.
                var lsClient = await EnsureLanguageServerAsync();

                // Wait 150 milliseconds in case the user is typing quickly.
                token.WaitHandle.WaitOne(150);
                if (token.IsCancellationRequested)
                {
                    _logger.Debug("Proposal request for {FilePath} was cancelled by the user.",
                        _relativeFilePath);
                    return null;
                }

                // Get code suggestion from language service
                var ret = await GetCodeSuggestionAsync(token);
                error = ret.error;

                if (error != null)
                {
                    _logger.Error("Error retrieving code suggestions for {FilePath}: {Error}", _relativeFilePath,
                        error);
                    return null;
                }

                if (ret.completion == null)
                {
                    _logger.Debug("No code suggestions available for {FilePath} at the requested position.",
                        _relativeFilePath);
                    return null;
                }

                if (string.IsNullOrWhiteSpace(ret.completion.insertText))
                {
                    _logger.Debug("Received empty suggestion text for {FilePath}.", _relativeFilePath);
                    _ = lsClient.SendGitlabTelemetryCodeSuggestionNotProvidedAsync(ret.completion.data.trackingId);
                    return null;
                }

                if (token.IsCancellationRequested)
                {
                    _logger.Debug(
                        "Proposal request for {FilePath} was cancelled by the user after proposals were returned.",
                        _relativeFilePath);

                    _ = lsClient.SendGitlabTelemetryCodeSuggestionCancelledAsync(ret.completion.data.trackingId);
                    return null;
                }

                var proposals = CreateProposalCollection(caret, completionState, ret.completion);
                _logger.Debug("Successfully created proposals for {FilePath}.", _relativeFilePath);

                return proposals;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error requesting proposals for {FilePath}.", _relativeFilePath);
                return null;
            }
            finally
            {
                _statusBar.CodeSuggestionsInProgressComplete();
                if (error != null)
                    _statusBar.CodeSuggestionsError(error);
            }
        }

        /// <summary>
        /// Called when a code suggestion has been accepted to commit the change to the buffer.
        /// </summary>
        /// <param name="suggestion"></param>
        private void CommitSuggestion(string suggestion)
        {
            _textView.TextBuffer.Insert(_textView.Caret.Position.BufferPosition, suggestion);
        }

        /// <summary>
        /// Create a GitlabProposalCollection from a code suggestion
        /// </summary>
        /// <param name="caret"></param>
        /// <param name="completionState"></param>
        /// <param name="suggestion"></param>
        /// <returns></returns>
        private ProposalCollection CreateProposalCollection(
            VirtualSnapshotPoint caret,
            CompletionState completionState, 
            CompletionItem suggestion)
        {
            // Create proposals
            const ProposalFlags flags =
                ProposalFlags.FormatAfterCommit |
                ProposalFlags.MoveCaretToEnd |
                ProposalFlags.DisableInIntelliSense |
                ProposalFlags.SimulateBraceCompletion;
            
            var displaySuggestion = suggestion.insertText;

            var bufferPosition = _textView.Caret.Position.BufferPosition;
            var line = bufferPosition.GetContainingLine();
            var column = bufferPosition.Position - line.Start.Position;

            // If the caret is in virtual space and the column position is zero,
            // it will look to the user like the caret is indented even through
            // the column is zero. Our code suggestion be based on a zero column
            // position and include the needed indentation.
            //
            // To make the display of the suggestion look correct to the user
            // we need to trim any preceding indentation. But only for the display
            // value. We want to insert with the correct indentation.
            if (_textView.Caret.InVirtualSpace && column == 0)
                displaySuggestion = displaySuggestion.TrimStart();

            var edits = new List<ProposedEdit>
            {
                new ProposedEdit(
                    _textView.GetTextElementSpan(_textView.Caret.Position.BufferPosition),
                    displaySuggestion)
            };

            var proposalId = "gitlab:" + suggestion.data.trackingId;
            var proposalMetadata =
                new GitLabProposalMetadata(
                    _workspaceId,
                    suggestion.data.trackingId);

            var proposals = new List<ProposalBase>
            {
                GitLabProposal.TryCreateProposal(
                    proposalMetadata,
                    null,
                    edits,
                    caret,
                    completionState,
                    flags,
                    proposalId: proposalId)
            };

            return new ProposalCollection(SourceName, proposals);
        }

        /// <summary>
        /// Call the Code Suggestion API and return a suggestion.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>Returns a suggestion or null</returns>
        private async Task<(CompletionItem completion, string error)> GetCodeSuggestionAsync(CancellationToken cancellationToken)
        {
            _logger.Debug("Requesting code suggestion for {FilePath} at line {Line} and column {Column}.", 
                _relativeFilePath, 
                _textView.Caret.Position.BufferPosition.GetContainingLine().LineNumber, 
                _textView.Caret.Position.BufferPosition.Position - _textView.Caret.Position.BufferPosition.GetContainingLine().Start.Position);

            var bufferPosition = _textView.Caret.Position.BufferPosition;
            var line = bufferPosition.GetContainingLine();
            var lineNumber = line.LineNumber;
            var column = bufferPosition.Position - line.Start.Position;

            if (cancellationToken.IsCancellationRequested)
            {
                _logger.Debug("Code suggestion request for {FilePath} was cancelled before execution.", _relativeFilePath);
                return (null, "Request cancelled");
            }

            var ret = await _lsClient.SendTextDocumentCompletionAsync(
                _relativeFilePath,
                (uint)lineNumber,
                (uint)column,
                cancellationToken);
            
            if (ret.Completions == null || ret.Completions.Length == 0)
            {
                _logger.Debug("No completions found for {FilePath} at line {Line} and column {Column}.", _relativeFilePath, lineNumber, column);
                return (null, ret.Error);
            }

            _logger.Debug("Received code suggestion for {FilePath}. Tracking ID: {TrackingId}", _relativeFilePath, ret.Completions[0].data.trackingId);
            return (ret.Completions[0], ret.Error);
        }
        
        private async Task<ILsClient> EnsureLanguageServerAsync()
        {
            if (_lsClient != null)
            {
                return _lsClient;
            }

            var clientStarted = await StartLanguageServerClientAsync();
            if (!clientStarted)
            {
                _logger.Error("Failed to start language server client during TextBuffer_Opened for {FilePath}.", _relativeFilePath);
                throw new Exception("Failed to start language server.");
            }
            _logger.Debug("Language server client started successfully for {FilePath}.", _relativeFilePath);

            return _lsClient;
        }

#pragma warning disable CS1998
        public override async Task DisposeAsync()
        {
            if (_disposed)
                return;

            _lsClient = null;
            _disposed = true;
            _codeSuggestionStateSubscription?.Dispose();
        }
#pragma warning restore CS1998
    }
}
