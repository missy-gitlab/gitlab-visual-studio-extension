﻿
namespace GitLab.Extension.CodeSuggestions
{
    public interface ILanguageManager
    {
        /// <summary>
        /// Get a filenames extension
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        string GetExtensionFromFilename(string filename);

        /// <summary>
        /// Check if content type is a known language with FeatureCodeSuggestions
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        bool CheckFeatureCodeSuggestion(string contentType, string extension);

        /// <summary>
        /// Get the language instance based on content type and file extension
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        Language GetLanguage(string contentType, string extension);
    }
}
