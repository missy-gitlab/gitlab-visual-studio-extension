using GitLab.Extension.GitLabApi.Model;

namespace GitLab.Extension.CodeSuggestions.State
{
    public static class Constants
    {
        public static readonly GitLabVersion CodeSuggestionsMinSupportedGitLabVersion =
            GitLabVersion.Parse("16.8.0");
        
        public static class Messages
        {
            private const string PleaseReviewLogs = "Please review logs for more information.";
            private const string PleaseVerifyConfiguration = "Please verify extension configuration.";
            
            public const string GeneralError = "An error occurred. " + PleaseReviewLogs;
            
            // API Error Messages
            public const string Unauthorized = "Unauthorized. " + PleaseVerifyConfiguration;
            
            // Settings Validation Messages
            public const string ExtensionNotConfigured = "Extension is not configured. " + PleaseVerifyConfiguration;
            public const string ConfigurationFailed = "Extension configuration failed. " + PleaseReviewLogs;
            public const string InvalidConfiguredUrl = "Configured GitLab URL is invalid. " + PleaseVerifyConfiguration;
            public const string InvalidAccessToken = "Configured access token is invalid. " + PleaseVerifyConfiguration;
            
            // Duo Not Provisioned For User Validation Messages
            public const string CodeSuggestionsNotProvisionedForUser = "Code Suggestions is now a paid feature, part of GitLab Duo Pro. To upgrade, contact your GitLab administrator.";
            
            // GitLab Version Validation Messages
            public const string UnsupportedGitLabVersion = "GitLab Duo Code Suggestions requires GitLab version 16.8 or later.";
            public const string UnsupportedGitLabVersionSelfHosted = UnsupportedGitLabVersion + " For more information about upgrading, see https://docs.gitlab.com/ee/update/.";
            
            // Workspace Validation Messages 
            public const string NoActiveWorkspace = "No active workspace.";
            public const string WorkspaceInitializing = "Workspace is initializing. Please wait.";
            public const string WorkspaceInitializationFailed = "Workspace failed to initialize. See logs for more information.";
            public const string CodeSuggestionDisabledForProject = "CodeSuggestions has been disabled for this project.";
        }
    }
}
