using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace GitLab.Extension.CodeSuggestions.State
{
    public class CodeSuggestionsEnabledStateProvider : ICodeSuggestionsEnabledStateProvider, IDisposable
    {
        private readonly IDisposable _subscription;
        
        private readonly BehaviorSubject<CodeSuggestionsEnabledState> _codeSuggestionsEnabledStateSubject =
            new BehaviorSubject<CodeSuggestionsEnabledState>(CodeSuggestionsEnabledState.Initial);

        public IObservable<CodeSuggestionsEnabledState> CodeSuggestionEnabledStateObservable =>
            _codeSuggestionsEnabledStateSubject.AsObservable();

        public IObservable<bool> IsCodeSuggestionsEnabledObservable =>
            CodeSuggestionEnabledStateObservable
                .Select(x => x.IsCodeSuggestionsEnabled);

        public CodeSuggestionsEnabledState CodeSuggestionsEnabledState =>
            _codeSuggestionsEnabledStateSubject.Value;

        public bool IsCodeSuggestionsEnabled =>
            CodeSuggestionsEnabledState.IsCodeSuggestionsEnabled;
        
        public CodeSuggestionsEnabledStateProvider(
            IEnumerable<ICodeSuggestionsEnabledValidator> validators)
        {
            var validatorList = validators.ToList();
            if (!validatorList.Any())
            {
                _codeSuggestionsEnabledStateSubject.OnNext(CodeSuggestionsEnabledState.Enabled);
                return;
            }
            
            _subscription =
                validatorList
                    .Select(validator => validator.ValidationResultObservable)
                    .CombineLatest(GetFirstDisabledStateOrEnabled)
                    .DistinctUntilChanged()
                    .Subscribe(_codeSuggestionsEnabledStateSubject);
        }
        
        private static CodeSuggestionsEnabledState GetFirstDisabledStateOrEnabled(
            IList<CodeSuggestionsEnabledState> states)
        {
            return states.FirstOrDefault(state => !state.IsCodeSuggestionsEnabled)
                   ?? CodeSuggestionsEnabledState.Enabled;
        }

        public void Dispose()
        {
            _subscription?.Dispose();
            _codeSuggestionsEnabledStateSubject?.Dispose();
        }
    }
}
