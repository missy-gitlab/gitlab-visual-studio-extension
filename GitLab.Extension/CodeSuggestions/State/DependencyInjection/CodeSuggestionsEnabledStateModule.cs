using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using GitLab.Extension.CodeSuggestions.State.Validators;

namespace GitLab.Extension.CodeSuggestions.State.DependencyInjection
{
    public class CodeSuggestionsEnabledStateModule : Module
    {
        private static readonly List<Type> ValidatorTypes =
            new List<Type>
            {
                typeof(SettingsConfiguredValidator),
                typeof(SettingsGitLabUriValidator),
                typeof(UserEnabledValidator),
                typeof(UserProvisionedValidator),
                typeof(MinimumGitLabVersionValidator),
                typeof(WorkspaceValidator),
                typeof(LsTokenCheckValidator)
            };
        
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            
            builder
                .RegisterType<CodeSuggestionsEnabledStateProvider>()
                .As<ICodeSuggestionsEnabledStateProvider>()
                .SingleInstance();
            
            builder
                .Register(c => c.Resolve<ICodeSuggestionsEnabledStateProvider>()
                    .CodeSuggestionEnabledStateObservable)
                .As<IObservable<CodeSuggestionsEnabledState>>();
            
            foreach (var validatorType in ValidatorTypes)
            {
                builder
                    .RegisterType(validatorType)
                    .As(validatorType)
                    .As<ICodeSuggestionsEnabledValidator>();
            }
            
            builder
                .Register(ctx => ValidatorTypes
                    .Select(ctx.Resolve)
                    .OfType<ICodeSuggestionsEnabledValidator>()
                    .ToList())
                .As<IEnumerable<ICodeSuggestionsEnabledValidator>>()
                .SingleInstance();
        }
    }
}
