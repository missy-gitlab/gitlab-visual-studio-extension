﻿namespace GitLab.Extension.CodeSuggestions.State
{
    public class CodeSuggestionsEnabledState
    {
        private CodeSuggestionsEnabledState(
            bool isCodeSuggestionsEnabled, 
            bool isStatusBarClickHandlerDisabled = false,
            string disabledMessage = null)
        {
            IsCodeSuggestionsEnabled = isCodeSuggestionsEnabled;
            IsStatusBarClickHandlerDisabled = isStatusBarClickHandlerDisabled;
            DisabledMessage = disabledMessage;
        }

        public bool IsCodeSuggestionsEnabled { get; }
        public bool IsStatusBarClickHandlerDisabled { get; }
        public string DisabledMessage { get; }

        public static readonly CodeSuggestionsEnabledState Initial =
            Disabled(
                disabledMessage: "Initializing",
                disableStatusBarClickHandler: true);
        
        public static readonly CodeSuggestionsEnabledState Enabled =
            new CodeSuggestionsEnabledState(true);

        public static CodeSuggestionsEnabledState Disabled(
            string disabledMessage = null,
            bool disableStatusBarClickHandler = false)
        {
            return new CodeSuggestionsEnabledState(
                false,
                disableStatusBarClickHandler,
                disabledMessage);
        }
    }
}
