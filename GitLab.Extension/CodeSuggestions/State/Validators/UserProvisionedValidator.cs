using System;
using System.Reactive.Linq;
using GitLab.Extension.CodeSuggestions.State.Utilities;
using GitLab.Extension.GitLabApi;
using GitLab.Extension.GitLabApi.Extensions;
using GitLab.Extension.SettingsUtil;
using GitLab.Extension.Utility.Results;
using GitLab.Extension.Utility.Results.Errors;

namespace GitLab.Extension.CodeSuggestions.State.Validators
{
    public class UserProvisionedValidator : ICodeSuggestionsEnabledValidator
    {
        public IObservable<CodeSuggestionsEnabledState> ValidationResultObservable { get; }

        public UserProvisionedValidator(
            IObservable<ISettings> settingsObservable,
            IGitLabClientFactory gitLabClientFactory)
        {
            ValidationResultObservable =
                settingsObservable
                    .DistinctUntilChanged(x => (x.GitLabUrl, x.GitLabAccessToken))
                    .SelectMany(settings => ValidateUserProvisioned(settings, gitLabClientFactory))
                    .Select(result =>
                        result.GetValueOrMapError(
                            CodeSuggestionEnabledStateUtilities.CreateCodeSuggestionStateFromError));
        }

        private static IObservable<Result<CodeSuggestionsEnabledState>> ValidateUserProvisioned(
            ISettings settings,
            IGitLabClientFactory gitLabClientFactory)
        {
            return Observable
                .FromAsync(async ct =>
                {
                    return await gitLabClientFactory
                        .CreateClient(settings)
                        .Map(client => client.IsCodeSuggestionsEnabledForUserAsync(ct))
                        .Bind(result =>
                            result.Map(
                                isEnabled => isEnabled
                                    ? CodeSuggestionsEnabledState.Enabled
                                    : CodeSuggestionsEnabledState.Disabled(
                                        Constants.Messages.CodeSuggestionsNotProvisionedForUser)));
                })
                .Catch<Result<CodeSuggestionsEnabledState>, Exception>(ex =>
                    Observable.Return(
                        Result.Error<CodeSuggestionsEnabledState>(
                            new ExceptionalError(ex))));
        }
    }
}
