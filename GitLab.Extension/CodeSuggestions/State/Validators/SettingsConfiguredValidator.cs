using System;
using System.Reactive.Linq;
using GitLab.Extension.SettingsUtil;

namespace GitLab.Extension.CodeSuggestions.State.Validators
{
    public class SettingsConfiguredValidator : ICodeSuggestionsEnabledValidator
    {
        public IObservable<CodeSuggestionsEnabledState> ValidationResultObservable { get; }

        public SettingsConfiguredValidator(
            IObservable<ISettings> settingsObservable)
        {
            ValidationResultObservable =
                settingsObservable
                    .Select(x => x.Configured)
                    .Select(isEnabled =>
                        isEnabled
                            ? CodeSuggestionsEnabledState.Enabled
                            : CodeSuggestionsEnabledState.Disabled(
                                Constants.Messages.ExtensionNotConfigured,
                                true));
        }
    }
}
