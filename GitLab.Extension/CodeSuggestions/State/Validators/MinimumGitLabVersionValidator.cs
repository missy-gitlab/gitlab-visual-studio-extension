using System;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using GitLab.Extension.CodeSuggestions.State.Utilities;
using GitLab.Extension.GitLabApi;
using GitLab.Extension.GitLabApi.Extensions;
using GitLab.Extension.SettingsUtil;
using GitLab.Extension.Utility.Results;

namespace GitLab.Extension.CodeSuggestions.State.Validators
{
    public class MinimumGitLabVersionValidator : ICodeSuggestionsEnabledValidator
    {
        public IObservable<CodeSuggestionsEnabledState> ValidationResultObservable { get; }

        public MinimumGitLabVersionValidator(
            IObservable<ISettings> settingsObservable,
            IGitLabClientFactory gitLabClientFactory)
        {
            ValidationResultObservable =
                settingsObservable
                    .DistinctUntilChanged(x => (x.GitLabUrl, x.GitLabAccessToken))
                    .SelectMany(settings => ValidateVersionCompatability(settings, gitLabClientFactory))
                    .Select(result =>
                        result.GetValueOrMapError(
                            CodeSuggestionEnabledStateUtilities.CreateCodeSuggestionStateFromError));
        }

        private static IObservable<Result<CodeSuggestionsEnabledState>> ValidateVersionCompatability(
            ISettings settings,
            IGitLabClientFactory gitLabClientFactory)
        {
            return Observable.FromAsync(cancellationToken =>
            {
                return gitLabClientFactory
                    .CreateClient(settings)
                    .Bind(client => ValidateGitLabVersionAsync(client, cancellationToken));
            });
        }

        private static Task<Result<CodeSuggestionsEnabledState>> ValidateGitLabVersionAsync(
            IGitLabClient client,
            CancellationToken cancellationToken)
        {
            return client
                .GetMetadataAsync(cancellationToken)
                .Map(metadata => metadata.Version >= Constants.CodeSuggestionsMinSupportedGitLabVersion)
                .Map(isSupportedVersion =>
                {
                    if (isSupportedVersion)
                    {
                        return CodeSuggestionsEnabledState.Enabled;
                    }

                    var messages =
                        client.IsSelfHosted
                            ? Constants.Messages.UnsupportedGitLabVersionSelfHosted
                            : Constants.Messages.UnsupportedGitLabVersion;

                    return CodeSuggestionsEnabledState.Disabled(messages, true);
                });
        }
    }
}
