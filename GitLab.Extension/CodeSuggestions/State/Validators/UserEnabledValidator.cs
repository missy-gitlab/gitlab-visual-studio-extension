using System;
using System.Reactive.Linq;
using GitLab.Extension.SettingsUtil;

namespace GitLab.Extension.CodeSuggestions.State.Validators
{
    public class UserEnabledValidator : ICodeSuggestionsEnabledValidator
    {
        public IObservable<CodeSuggestionsEnabledState> ValidationResultObservable { get; }

        public UserEnabledValidator(
            IObservable<ISettings> settingsObservable)
        {
            ValidationResultObservable =
                settingsObservable
                    .Select(x => x.IsCodeSuggestionsEnabled
                        ? CodeSuggestionsEnabledState.Enabled
                        : CodeSuggestionsEnabledState.Disabled());
        }
    }
}
