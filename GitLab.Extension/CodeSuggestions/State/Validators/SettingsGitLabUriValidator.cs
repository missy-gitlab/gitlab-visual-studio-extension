using System;
using System.Reactive.Linq;
using GitLab.Extension.SettingsUtil;

namespace GitLab.Extension.CodeSuggestions.State.Validators
{
    public class SettingsGitLabUriValidator : ICodeSuggestionsEnabledValidator
    {
        public IObservable<CodeSuggestionsEnabledState> ValidationResultObservable { get; }

        public SettingsGitLabUriValidator(
            IObservable<ISettings> settingsObservable)
        {
            ValidationResultObservable =
                settingsObservable
                    .Select(settings =>
                        !Uri.TryCreate(settings.GitLabUrl, UriKind.Absolute, out _)
                            ? CodeSuggestionsEnabledState.Disabled(Constants.Messages.InvalidConfiguredUrl)
                            : CodeSuggestionsEnabledState.Enabled);
        }
    }
}
