using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using GitLab.Extension.GitLabApi;
using GitLab.Extension.GitLabApi.Extensions;
using GitLab.Extension.GitLabApi.Model;
using GitLab.Extension.SettingsUtil;
using GitLab.Extension.Utility.Git;
using GitLab.Extension.Utility.Results;
using GitLab.Extension.Workspace;

namespace GitLab.Extension.CodeSuggestions.State.Validators
{
    public class WorkspaceValidator : ICodeSuggestionsEnabledValidator
    {
        public IObservable<CodeSuggestionsEnabledState> ValidationResultObservable { get; }

        public WorkspaceValidator(
            IObservable<ISettings> settingsObservable,
            IActiveWorkspaceStateObservable activeWorkspaceStateObservable,
            IGitLabClientFactory clientFactory,
            IRepositoryFactory repositoryFactory)
        {
            ValidationResultObservable =
                settingsObservable
                    .DistinctUntilChanged(x => (x.GitLabUrl, x.GitLabAccessToken))
                    .CombineLatest(activeWorkspaceStateObservable,
                        (settings, workspace) => new { Settings = settings, WorkspaceState = workspace })
                    .SelectMany(x => Observable.FromAsync(() =>
                        ResolveIsDuoEnabledAsync(x.Settings, x.WorkspaceState, clientFactory, repositoryFactory)));
        }

        private static async Task<CodeSuggestionsEnabledState> ResolveIsDuoEnabledAsync(
            ISettings settings,
            WorkspaceState activeWorkspaceState,
            IGitLabClientFactory clientFactory,
            IRepositoryFactory repositoryFactory)
        {
            return activeWorkspaceState switch
            {
                NoActiveWorkspace _ => CodeSuggestionsEnabledState.Disabled(Constants.Messages.NoActiveWorkspace, true),
                WorkspaceInitializing _ => CodeSuggestionsEnabledState.Disabled(Constants.Messages.WorkspaceInitializing,
                    true),
                WorkspaceInitializationFailed _ => CodeSuggestionsEnabledState.Disabled(
                    Constants.Messages.WorkspaceInitializationFailed, true),
                WorkspaceActive active => await ResolveIsDuoEnabledForWorkspaceAsync(settings, active.Workspace,
                    clientFactory, repositoryFactory),
                _ => CodeSuggestionsEnabledState.Disabled(Constants.Messages.GeneralError, true)
            };
        }

        private static async Task<CodeSuggestionsEnabledState> ResolveIsDuoEnabledForWorkspaceAsync(
            ISettings settings,
            IWorkspace workspace,
            IGitLabClientFactory clientFactory,
            IRepositoryFactory repositoryFactory)
        {
            var clientResult = clientFactory.CreateClient(settings);
            
            var enabledRequestResult =
                repositoryFactory
                    .GetRepository(workspace.Id.SolutionPath)
                    .Bind(gitRepository =>
                    {
                        using (gitRepository)
                        {
                            return gitRepository
                                .GetRemoteInfo();
                        }
                    })
                    .Map(x => new CodeSuggestionsEnabledRequest
                    {
                        ProjectPath = x.FullPath
                    });

            return await clientResult
                .Zip(enabledRequestResult)
                .Map(tuple =>
                {
                    var (client, request) = tuple;
                    return client.IsCodeSuggestionsEnabledForProjectAsync(request);
                })
                .Bind(result =>
                    result.Map(isEnabled => isEnabled
                        ? CodeSuggestionsEnabledState.Enabled
                        : CodeSuggestionsEnabledState.Disabled(Constants.Messages.CodeSuggestionDisabledForProject))
                )
                .GetValueOrMapErrorAsync(_ => CodeSuggestionsEnabledState.Enabled);
        }
    }
}
