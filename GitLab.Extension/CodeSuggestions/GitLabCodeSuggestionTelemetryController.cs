using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GitLab.Extension.CodeSuggestions.Model;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.Workspace;
using Microsoft.VisualStudio.Shell;
using Serilog;

namespace GitLab.Extension.CodeSuggestions
{
    public class GitLabCodeSuggestionTelemetryController :
        IGitLabCodeSuggestionTelemetryController
    {
        private readonly IWorkspaceLsClientProvider _lsClientProvider;
        private readonly ILogger _logger;

        private readonly ConcurrentDictionary<string, GitLabProposalMetadata> _activeProposals =
            new ConcurrentDictionary<string, GitLabProposalMetadata>();
        
        public GitLabCodeSuggestionTelemetryController(
            IWorkspaceLsClientProvider lsClientProvider,
            ILogger logger)
        {
            _lsClientProvider = lsClientProvider;
            _logger = logger;
        }

        public async Task RejectAllManagedProposalsAsync()
        {
            var tasks = 
                _activeProposals.Values.Select(
                    proposalInfo => 
                        HandleProposalAsync(
                            proposalInfo, 
                            GitLabProposalState.Rejected));

            await Task.WhenAll(tasks);
        }
        
        public Task OnShownAsync(
            GitLabProposalMetadata proposalMetadata) =>
            HandleProposalAsync(proposalMetadata, GitLabProposalState.Shown);

        public Task OnRejectedAsync(
            GitLabProposalMetadata proposalMetadata) => 
            HandleProposalAsync(proposalMetadata, GitLabProposalState.Rejected);

        public Task OnAcceptedAsync(
            GitLabProposalMetadata proposalMetadata) =>
            HandleProposalAsync(proposalMetadata, GitLabProposalState.Accepted);

        private async Task HandleProposalAsync(
            GitLabProposalMetadata proposalMetadata,
            GitLabProposalState action)
        {
            try
            {
                var shouldProceed = action == GitLabProposalState.Shown
                    ? _activeProposals.TryAdd(proposalMetadata.TelemetryTrackingId, proposalMetadata)
                    : _activeProposals.TryRemove(proposalMetadata.TelemetryTrackingId, out var _);

                if (!shouldProceed)
                {
                    _logger.Debug($"Proposal with TelemetryId {proposalMetadata.TelemetryTrackingId} already processed for {action} action.");
                    return;
                }

                var lsClient = _lsClientProvider.GetClient(proposalMetadata.WorkspaceId);
                await SendTelemetryActionAsync(lsClient, proposalMetadata.TelemetryTrackingId, action);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"Error while processing telemetry for proposal with TelemetryId {proposalMetadata.TelemetryTrackingId} for {action} action.");
            }
        }
        
        private static Task SendTelemetryActionAsync(
            ILsClient lsClient,
            string telemetryId,
            GitLabProposalState action)
        {
            switch (action)
            {
                case GitLabProposalState.Shown:
                    return lsClient.SendGitlabTelemetryCodeSuggestionShownAsync(telemetryId);
                case GitLabProposalState.Rejected:
                    return lsClient.SendGitlabTelemetryCodeSuggestionRejectedAsync(telemetryId);
                case GitLabProposalState.Accepted:
                    return lsClient.SendGitlabTelemetryCodeSuggestionAcceptedAsync(telemetryId);
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }
        }
    }
}
