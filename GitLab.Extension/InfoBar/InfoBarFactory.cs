﻿using GitLab.Extension.VS;
using Microsoft.VisualStudio.Imaging.Interop;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Serilog;
using System.Collections.Generic;
using System.Linq;

namespace GitLab.Extension.InfoBar
{
    public class InfoBarFactory : IInfoBarFactory
    {
        private ILogger _logger;

        public InfoBarFactory(
            ILogger logger)
        {
            _logger = logger;            
        }

        public IInfoBar AttachInfoBar(
            IVsInfoBarHost host,
            string message,
            IEnumerable<InfoBarAction> actions,
            ImageMoniker imageMoniker = default)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            var model =
                CreateInfoBarModel(
                    message,
                    actions,
                    imageMoniker);

            var uiElement = CreateUIElement(model);
            if (uiElement == null)
            {
                return null;
            }

            host.AddInfoBar(uiElement);

            var actionsByKey =
                actions
                    .Where(x => x.Action != null)
                    .ToDictionary(x => x.Label, x => x.Action);

            return new InfoBar(
                _logger,
                host,
                uiElement,
                actionsByKey);
        }

        private InfoBarModel CreateInfoBarModel(
            string message,
            IEnumerable<InfoBarAction> actions,
            ImageMoniker imageMoniker)
        {
            var infoBarMessages = new[] { new InfoBarTextSpan(message) };
            var infoBarActions = actions.Select(x => new InfoBarHyperlink(x.Label)).ToArray();

            return new InfoBarModel(
                infoBarMessages,
                infoBarActions,
                image: imageMoniker,
                isCloseButtonVisible: true);
        }

        private static IVsInfoBarUIElement CreateUIElement(
            InfoBarModel model)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            var infoBarUIFactory = (IVsInfoBarUIFactory) GlobalServiceProvider.GetService(typeof(SVsInfoBarUIFactory));
            if (infoBarUIFactory == null)
            {
                return null;
            }

            var uiElement = infoBarUIFactory.CreateInfoBar(model);
            if (uiElement == null)
            {
                return null;
            }

            return uiElement;
        }
    }
}
