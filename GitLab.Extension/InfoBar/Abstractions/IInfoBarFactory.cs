﻿using Microsoft.VisualStudio.Imaging.Interop;
using Microsoft.VisualStudio.Shell.Interop;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace GitLab.Extension.InfoBar
{
    public interface IInfoBarFactory
    {
        IInfoBar AttachInfoBar(
            IVsInfoBarHost host,
            string message,
            IEnumerable<InfoBarAction> action,
            ImageMoniker imageMoniker = default);
    }
}
