﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Serilog;

namespace GitLab.Extension.InfoBar
{
    internal class InfoBar : IInfoBar, IVsInfoBarUIEvents
    {
        private readonly IReadOnlyDictionary<string, Action> _actions;
        private readonly IVsInfoBarHost _host;
        private readonly ILogger _logger;
        private readonly IVsInfoBarUIElement _uiElement;
        
        private uint? _cookie;
        private bool _isClosed;

        public InfoBar(
            ILogger logger,
            IVsInfoBarHost host,
            IVsInfoBarUIElement uiElement,
            IReadOnlyDictionary<string, Action> actions)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            _logger = logger;
            _host = host;
            _uiElement = uiElement;
            _actions = actions;

            Advise();
        }

        #region IInfoBar

        public event EventHandler Closed;

        public void Close()
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            if (_isClosed)
            {
                _logger.Warning($"{nameof(Close)}: Attempting to close an already closed InfoBar");
                return;
            }

            _uiElement.Close();
            _host.RemoveInfoBar(_uiElement);
        }

        #endregion

        #region IVsInfoBarUIEvents

        public void OnClosed(
            IVsInfoBarUIElement infoBarUIElement)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            
            Unadvise();
            _isClosed = true;
            Closed?.Invoke(this, EventArgs.Empty);
        }

        public void OnActionItemClicked(
            IVsInfoBarUIElement infoBarUIElement,
            IVsInfoBarActionItem actionItem)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            if (!_actions.TryGetValue(actionItem.Text, out var action))
            {
                _logger.Warning(
                    $"{nameof(OnActionItemClicked)}: Action '{actionItem.Text}' not found in the actions dictionary");
                return;
            }

            action.Invoke();
        }

        #endregion

        #region Helpers

        private void Advise()
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            if (ErrorHandler.Succeeded(_uiElement.Advise(this, out var syncCookie)))
            {
                _cookie = syncCookie;
            }
            else
            {
                _logger.Warning(
                    $"{nameof(Advise)}: Failed to advise the UI element, this warning can be safely ignored");
            }
        }

        private void Unadvise()
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            if (!_cookie.HasValue)
            {
                // already unadvised
                return;
            }

            if (ErrorHandler.Succeeded(_uiElement.Unadvise(_cookie.Value)))
            {
                _cookie = null;
            }
            else
            {
                _logger.Warning(
                    $"{nameof(Unadvise)}: Failed to unadvise the UI element, this warning can be safely ignored");
            }
        }

        #endregion
    }
}
