namespace GitLab.Extension.Utility.Git.Errors
{
    public class GitRepositoryNotFoundError : Results.Errors.Error
    {
        public GitRepositoryNotFoundError(
            string directory) : base($"Directory at path '{directory}' does not lead to a git repository")
        {
        }
    }
}