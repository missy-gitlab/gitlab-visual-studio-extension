using GitLab.Extension.Utility.Results;
using LibGit2Sharp;

namespace GitLab.Extension.Utility.Git
{
    public interface IRepositoryFactory
    {
        Result<IRepository> GetRepository(
            string path);
    }
}