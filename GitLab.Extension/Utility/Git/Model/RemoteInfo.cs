﻿using System;
using System.Collections.Generic;
using System.Linq;
using GitLab.Extension.Utility.Results;
using GitLab.Extension.Utility.Results.Errors;

namespace GitLab.Extension.Utility.Git.Model
{
    public class RemoteInfo
    {
        public string Namespace { get; }
        public IReadOnlyList<string> Groups { get; }
        public string Project { get; }
        public string FullPath { get; }

        private RemoteInfo(
            string @namespace,
            IReadOnlyList<string> groups,
            string project)
        {
            Namespace = @namespace;
            Groups = groups;
            Project = project;

            var pathSegments =
                new[] { @namespace }
                    .Concat(groups)
                    .Append(project)
                    .ToArray();

            FullPath = string.Join("/", pathSegments);
        }

        public static Result<RemoteInfo> ParseRemoteUrl(
            string remoteUrl)
        {
            if (!Uri.TryCreate(remoteUrl, UriKind.Absolute, out Uri remoteUri))
            {
                return Result<RemoteInfo>.Error(
                    new Error("Invalid URL format"));
            }
            
            var path = remoteUri.AbsolutePath.TrimStart('/');
            path =
                path.EndsWith(".git", StringComparison.OrdinalIgnoreCase)
                    ? path.Substring(0, path.Length - 4)
                    : path;

            var parts = path.Split('/');
            if (parts.Length < 2)
            {
                return Result.Error<RemoteInfo>(
                    new Error("URL does not enough segments"));
            }
            
            var namespacePart = parts[0];
            var project = parts.Last();
            var groups = parts.Skip(1).Take(parts.Length - 2).ToArray();
                    
            return Result<RemoteInfo>.Ok(
                new RemoteInfo(namespacePart, groups, project));
        }
    }
}