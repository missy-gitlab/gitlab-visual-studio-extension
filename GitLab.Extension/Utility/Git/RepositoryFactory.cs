using System;
using System.IO;
using GitLab.Extension.Utility.Git.Errors;
using GitLab.Extension.Utility.Results;
using LibGit2Sharp;

namespace GitLab.Extension.Utility.Git
{
    public class RepositoryFactory : IRepositoryFactory
    {
        public Result<IRepository> GetRepository(
            string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return Result.Error<IRepository>(
                    new ArgumentException("The repository path cannot be null or whitespace. ", nameof(path)));
            }

            if (!Directory.Exists(path))
            {
                return Result.Error<IRepository>(
                    new DirectoryNotFoundException($"The specified directory '{path}' was not found"));
            }

            try
            {
                if (Repository.IsValid(path))
                {
                    return Result<IRepository>.Ok(
                        new Repository(path));
                }
                else
                {
                    return Result.Error<IRepository>(
                        new GitRepositoryNotFoundError(path));
                }
            }
            catch (Exception ex)
            {
                return Result.Error<IRepository>(
                    new Exception(
                        $"An exception occurred while accessing the repository at '{path}': {ex.Message}",
                        ex));
            }
        }
    }
}