using System.Collections.Generic;

namespace GitLab.Extension.Utility.Results
{
    public interface IReason
    {
        string Message { get; }
        IReadOnlyDictionary<string, object> Metadata { get; }
    }
}