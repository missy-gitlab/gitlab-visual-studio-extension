using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace GitLab.Extension.Utility.Results
{
    [SuppressMessage("Style", "VSTHRD200:Use \"Async\" suffix for async methods")]
    [SuppressMessage("Usage", "VSTHRD003:Avoid awaiting foreign Tasks")]
    public static class ResultExtensions
    {
        public static T GetValueOrDefault<T>(
            this Result<T> result) =>
            result.Match(x => x, _ => default);

        public static async Task<T> GetValueOrDefaultAsync<T>(
            this Task<Result<T>> result)
        {
            return (await result)
                .Match(x => x, _ => default);
        }
        
        public static T GetValueOrMapError<T>(
            this Result<T> result,
            Func<IError, T> error)
        {
            return result.Match(x => x, error);
        }

        public static Task<T> GetValueOrMapErrorAsync<T>(
            this Task<Result<T>> result,
            Func<IError, T> error)
        {
            return result.Match(x => x, error);
        }

        #region match

        public static void Match<T>(
            this Result<T> result,
            Action<T> ok,
            Action<IError> error = null) => result.Match(
            x =>
            {
                ok.Invoke(x);
                return true;
            },
            err =>
            {
                error?.Invoke(err);
                return true;
            });
        
        public static Task<T1> Match<T, T1>(
            this Result<T> result,
            Func<T, Task<T1>> ok,
            Func<IError, T1> err) =>
            result.Match(ok, e => Task.FromResult(err(e)));

        public static Task<T1> Match<T, T1>(
            this Result<T> result,
            Func<T, Task<T1>> ok,
            Func<IError, Task<T1>> err) =>
            result.Match(ok, err);

        public static async Task<T1> Match<T, T1>(
            this Task<Result<T>> result,
            Func<T, T1> ok,
            Func<IError, T1> err) =>
            (await result).Match(ok, err);
        
        public static async Task<T1> Match<T, T1>(
            this Task<Result<T>> result,
            Func<T, Task<T1>> ok,
            Func<IError, Task<T1>> err) =>
            await (await result).Match(ok, err);
        
        public static async Task<T1> Match<T, T1>(
            this Task<Result<T>> result,
            Func<T, Task<T1>> ok,
            Func<IError, T1> err) =>
            await (await result).Match(ok, e => Task.FromResult(err(e)));

        #endregion
        
        #region bind

        public static Result<T1> Bind<T, T1>(
            this Result<T> result,
            Func<T, Result<T1>> bind) =>
            result.Match(bind, Result.Error<T1>);
        
        public static Task<Result<T1>> Bind<T, T1>(
            this Result<T> result,
            Func<T, Task<Result<T1>>> bind) =>
            result.Match(bind, error => Task.FromResult(Result.Error<T1>(error)));

        public static async Task<Result<T1>> Bind<T, T1>(
            this Task<Result<T>> result,
            Func<T, Result<T1>> bind) =>
            (await result).Bind(bind);
        
        public static async Task<Result<T1>> Bind<T, T1>(
            this Task<Result<T>> result,
            Func<T, Task<Result<T1>>> bind) =>
            await (await result).Bind(bind);

        #endregion
        
        #region map

        public static Result<T1> Map<T, T1>(
            this Result<T> result,
            Func<T, T1> map) => 
            result.Bind(x => Result.Ok(map(x)));

        public static Task<Result<T1>> Map<T, T1>(
            this Result<T> result,
            Func<T, Task<T1>> map) => 
            result.Bind(async x => Result.Ok(await map(x)));

        public static async Task<Result<T1>> Map<T, T1>(
            this Task<Result<T>> result,
            Func<T, T1> map) =>
            (await result).Map(map);
        
        public static async Task<Result<T1>> Map<T, T1>(
            this Task<Result<T>> result,
            Func<T, Task<T1>> map) =>
            await (await result).Map(map);

        #endregion

        #region zip

        public static Result<(T, T1)> Zip<T, T1>(
            this Result<T> first,
            Result<T1> second)
        {
            return first
                .Match(
                    x =>
                    {
                        return second.Match(
                            y => Result.Ok<(T, T1)>((x, y)),
                            Result.Error<(T, T1)>);
                    },
                    Result.Error<(T, T1)>);
        }
        
        public static async Task<Result<(T, T1)>> Zip<T, T1>(
            this Result<T> first,
            Task<Result<T1>> secondTask)
        {
            var second = await secondTask;
            return first.Zip(second);
        }
        
        public static async Task<Result<(T, T1)>> Zip<T, T1>(
            this Task<Result<T>> firstTask,
            Task<Result<T1>> secondTask)
        {
            var first = await firstTask;
            var second = await secondTask;
            return first.Zip(second);
        }
        
        public static async Task<Result<(T, T1)>> Zip<T, T1>(
            this Task<Result<T>> firstTask,
            Func<T, Task<Result<T1>>> secondFunc)
        {
            var firstResult = await firstTask;
            return await firstResult.Match(
                async x => {
                    var secondResult = await secondFunc(x);
                    return secondResult.Match(
                        y => Result.Ok((x, y)),
                        Result.Error<(T, T1)>
                    );
                },
                err => Task.FromResult(Result.Error<(T, T1)>(err))
            );
        }
        
        public static async Task<Result<(T, T1)>> Zip<T, T1>(
            this Result<T> first,
            Func<T, Task<Result<T1>>> secondFunc)
        {
            return await first.Match(
                async x => {
                    var secondResult = await secondFunc(x);
                    return secondResult.Match(
                        y => Result.Ok((x, y)),
                        Result.Error<(T, T1)>
                    );
                },
                err => Task.FromResult(Result.Error<(T, T1)>(err))
            );
        }

        #endregion
    }
}