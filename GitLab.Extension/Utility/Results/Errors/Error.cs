using System.Collections.Generic;
using System.Linq;
using GitLab.Extension.Utility.Results.Utilities;

namespace GitLab.Extension.Utility.Results.Errors
{
    public class Error : IError
    {
        public string Message { get; protected set; }
        public IReadOnlyDictionary<string, object> Metadata { get; set; }
        public IReadOnlyCollection<IError> Reasons { get; set; }

        public Error(
            string message = null,
            IEnumerable<IError> reasons = null,
            IReadOnlyDictionary<string, object> metadata = null)
        {
            Message = message;
            Reasons = reasons?.ToList() ?? new List<IError>();
            Metadata = metadata ?? new Dictionary<string, object>();
        }

        public override string ToString()
        {
            return new ReasonStringBuilder()
                .WithReasonType(GetType())
                .WithInfo(nameof(Message), Message)
                .WithInfo(nameof(Metadata), string.Join("; ", Metadata))
                .WithInfo(nameof(Reasons), string.Join("; ", Reasons))
                .Build();
        }
    }
}