﻿namespace GitLab.Extension.LanguageServer.Models
{
    public class TokenValidationNotification
    {
        public string reason;
        public string message;
    }
}
