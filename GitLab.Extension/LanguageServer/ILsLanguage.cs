﻿
namespace GitLab.Extension.LanguageServer
{

    public interface ILsLanguage
    {
        LsLanguageId LanguageId { get; }
    }
}
