﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLab.Extension.Constants
{
    internal static class CodeAnalysisSuppressionJustification
    {
        public const string AlreadyOnMainThread = "We are already assured to be on the main thread";
    }
}
