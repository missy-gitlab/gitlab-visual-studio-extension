﻿using System.Threading.Tasks;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.Command
{
    public interface ICommandInitializer
    {
        Task InitializeAsync(
            AsyncPackage package);
    }
}
