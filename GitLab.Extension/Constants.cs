﻿using GitLab.Extension.GitLabApi.Model;

namespace GitLab.Extension
{
    public static class Constants
    {
        public static readonly GitLabVersion CodeSuggestionsMinSupportedGitLabVersion = GitLabVersion.Parse("16.8.0");

        public static class Messages
        {
            public const string PleaseReviewLogs =
                "Please review logs for more information.";
            
            public const string ExtensionNotConfigured =
                "Extension is not configured.";

            public const string ConfigurationFailed =
                "Extension configuration failed. " + PleaseReviewLogs;

            public const string InvalidAccessToken =
                "Configured access token is invalid. " + PleaseReviewLogs;
            
            public const string UnsupportedGitLabVersion = 
                "GitLab Duo Code Suggestions requires GitLab version 16.8 or later.";

            public const string UnsupportedGitLabVersionSelfHosted = 
                UnsupportedGitLabVersion + " Please refer to https://docs.gitlab.com/ee/update/ for more information on upgrading.";

            public const string CodeSuggestionsNotProvisionedForUser = 
                "Code Suggestions is now a paid feature, part of GitLab Duo Pro. To upgrade, contact your GitLab administrator.";
        }
    }
}
