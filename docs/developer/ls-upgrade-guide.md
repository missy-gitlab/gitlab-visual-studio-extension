# Language Server upgrade guide

This document outlines the process for upgrading the language server in the
`GitLab.Extension` VSIX extension. The goal is to ensure a seamless and efficient
upgrade process.

This upgrade process is manual, but might become more automated in the future.

## Upgrade the language server

The language server binaries are stored in the package registry of the `gitlab-lsp` project.

1. Access the package registry:
   1. Go to the [GitLab Language Server](https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp/) project.
   1. On the left sidebar, select **Deploy > Package Registry**.
   1. Filter the results to only `Generic` packages, as these contain the binary you need:
      1. Select **Filter results**.
      1. Select the parameters `Type`, `=`, `Generic`.
   1. Select the version of the package you want to use.
   1. In the **Assets** section, select `gitlab-lsp-win-x64.exe` to download it.
      This file is the language server binary you need for the Visual Studio extension.
1. To replace the existing binary:
   1. Go to the `~/GitLab.Extension/Resources/` directory in this repository.
   1. Replace the existing `gitlab-lsp-win-x64.exe` with the file you downloaded.
1. Build and launch the `GitLab.Extension` project to test the integration of the
   new Language Server binary. Verify its performance and compatability with the extension.

## Conclusion

Congratulations! You've successfully changed the version of the Language Server
used by the `GitLab.Extension` VSIX extension.
