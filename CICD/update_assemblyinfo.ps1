# This script gets the new build tag
# and then reads in the VSIX manifest file
# and then updates the Version to be the new build tag
# and finally saves the file.

try {
	$buildtagFile = '.\output\buildtag'
	$buildtag = Get-Content $buildtagFile -Raw -ErrorAction Stop
	$buildtag = $buildtag.TrimStart('BUILDTAG=').Trim()
# Call Resolve-Path to ensure we can save the file at the end
	$assemblyinfo = Resolve-Path '.\GitLab.Extension\Properties\AssemblyInfo.cs'
	(Get-Content $assemblyinfo).replace('1.0.0.0', $buildtag) | Set-Content $assemblyinfo
	Write-Output "The Version of $assemblyinfo has been updated to $buildtag."
}
catch {
	Write-Output "There was an error updating the $assemblyinfo. This is an expected failure when not on the main branch."
}
