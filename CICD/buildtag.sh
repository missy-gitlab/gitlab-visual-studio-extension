#!/bin/bash

#
# Called during:
#   - Stage: build
#   - Job..: build
#
# Generates:
#   - output/buildtag -- This file can be sourced in shell scripts to
#       provide the BUILDTAG variable. It also provides the build #
#       for the assets zip.
#
# Responsible for determining our current release version and buildtag.
#
# For master branch this is determined using the vsix.properties file
# and git describe to find the last tagged build and increment by one.
#
# For non-master branch build the version is always 0.0.0
#

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

source $DIR/common.sh
source $DIR/../vsix.properties

if [[ "$BRANCH_NAME" == "$CI_DEFAULT_BRANCH" || "${BRANCH_NAME//-[0987654321]}" == "prod" || "${BRANCH_NAME//-[0987654321].[0987654321]}" == "prod" ]]; then

  requires 'git'

  git fetch --tags
  git switch $CI_COMMIT_BRANCH

  last=$(git tag -l "$TAG_PREFIX$RELEASE_VERSION.*" --sort=v:refname | tail -1 | cut -f2 -d.)
  if [[ -z "$last" ]]; then
    # This error typically happens if you haven't created a feature branch.
    # For more information, see
    # https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/docs/user/troubleshooting.md
    echo "buildtag.sh: Error: unable to find last release."
    exit 1
  fi

  next=$((last+1))

  echo BUILDTAG=${RELEASE_VERSION}.${next}.0
  echo BUILDTAG=${RELEASE_VERSION}.${next}.0 > $DIR/../output/buildtag
  export BUILDTAG=${RELEASE_VERSION}.${next}.0

else

  echo BUILDTAG=0.0.0
  echo BUILDTAG=0.0.0 > $DIR/../output/buildtag
  export BUILDTAG=0.0.0

fi

# end
