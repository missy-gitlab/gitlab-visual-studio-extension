﻿using Microsoft.VisualStudio.Sdk.TestFramework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLab.Extension.Tests
{
    [SetUpFixture]
    public class AssemblySetup
    {
        internal static GlobalServiceProvider MockServiceProvider { get; private set; }

        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            MockServiceProvider = new GlobalServiceProvider();
        }

        [OneTimeTearDown]
        public void RunAfterAnyTests()
        {
            MockServiceProvider.Dispose();
        }
    }
}
