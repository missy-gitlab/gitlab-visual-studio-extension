﻿using GitLab.Extension.InfoBar;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Imaging.Interop;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GitLab.Extension.Tests.InfoBar
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "VSTHRD200:Use \"Async\" suffix for async methods", Justification = "Ending in async confuses naming")]
    [TestFixture]
    public class InfoBarFactoryExtensionsTests
    {
        private Mock<IInfoBarFactory> _infoBarFactoryMock;

        [SetUp]
        public void SetUp()
        {
            _infoBarFactoryMock = new Mock<IInfoBarFactory>();
        }

        [Test]
        [ExecuteOnMainThread]
        public void AttachInfoBarToMainWindow_ReturnsNull_WhenMainWindowInfoBarHostNotRetreived()
        {
            // Arrage
            AssemblySetup.MockServiceProvider.Reset();
            var mockShell = new Mock<IVsShell>();
            AssemblySetup.MockServiceProvider.AddService(typeof(IVsShell), mockShell.Object);

            // Act
            var result = 
                InfoBarFactoryExtensions.AttachInfoBarToMainWindow(
                    _infoBarFactoryMock.Object,
                    "Test message",
                    new List<InfoBarAction>());            


            // Assert 
            Assert.IsNull(result);
        }

        [Test]
        [ExecuteOnMainThread]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "VSTHRD010:Invoke single-threaded types on Main thread", Justification = "Test is already running on main thread")]
        public void AttachInfoBarToMainWindow_CallsAttachInfoBar_WhenMainWindowInfoBarHostRetrieved()
        {
            // Arrange
            AssemblySetup.MockServiceProvider.Reset();

            var message = "Test Message";
            var actions = new List<InfoBarAction>();

            var mockHost = new Mock<IVsInfoBarHost>();
            var mockShell = new Mock<IVsShell>();

            object _host = mockHost.Object;
            mockShell.Setup(shell => shell.GetProperty((int)__VSSPROPID7.VSSPROPID_MainWindowInfoBarHost, out _host))
                .Returns(VSConstants.S_OK);
            AssemblySetup.MockServiceProvider.AddService(typeof(IVsShell), mockShell.Object);

            // Act
            InfoBarFactoryExtensions.AttachInfoBarToMainWindow(
                _infoBarFactoryMock.Object, 
                message, 
                actions);

            // Assert
            _infoBarFactoryMock.Verify(x => x.AttachInfoBar(_host as IVsInfoBarHost, message, actions, It.IsAny<ImageMoniker>()), Times.Once());
        }
    }
}
