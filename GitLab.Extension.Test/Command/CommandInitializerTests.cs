﻿using GitLab.Extension.Command;
using Microsoft.VisualStudio.Shell;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GitLab.Extension.Tests.Command
{
    [TestFixture]
    public class CommandInitializerTests
    {
        [Test]
        public async Task InitializeAsync_InitializesAllCommandsAsync()
        {
            // Arrange
            var package = new Mock<AsyncPackage>();
            var commandA = new Mock<ICommand>();
            var commandB = new Mock<ICommand>();

            var commands = 
                new List<ICommand> { 
                    commandA.Object, 
                    commandB.Object };

            var commandInitializer = new CommandInitializer(commands);

            // Act
            await commandInitializer.InitializeAsync(package.Object);


            // Assert
            commandA.Verify(x => x.InitializeAsync(It.Is<AsyncPackage>(p => p == package.Object)), Times.Once);
            commandB.Verify(x => x.InitializeAsync(It.Is<AsyncPackage>(p => p == package.Object)), Times.Once);
        }

    }
}
