﻿using NUnit.Framework;
using System;
using System.IO;
using GitLab.Extension.SettingsUtil;
using static GitLab.Extension.Tests.SettingsTests;
using Serilog.Events;
using Autofac;
using Serilog;
using GitLab.Extension.LanguageServer;

namespace GitLab.Extension.Tests
{
    /// <summary>
    /// Data or settings needed to run tests.
    /// </summary>
    [SetUpFixture]
    public class TestData : TestBase
    {
        /// <summary>
        /// Token that can be provided to language server for code suggestions access.
        /// This is usually a personal access token with read_api and read_user privs.
        /// Make sure you have enabled code suggestions in your account settings.
        /// </summary>
        public static string CodeSuggestionsToken {get; private set;}

        /// <summary>
        /// Our solution name 'GitLab.Extension'
        /// </summary>
        public static string SolutionName { get; } = "GitLab.Extension";

        /// <summary>
        /// Path to our solutions folder.
        /// </summary>
        public static string SolutionPath { get; private set; }

        public static string GitLabUrl { get; private set; }

        public static LsImpl LanguageServerImpl { get; private set; } = LsImpl.TypeScript;

        /// <summary>
        /// Store configured access token
        /// </summary>
        private string _userAccessToken = string.Empty;

        /// <summary>
        /// Store configured code suggestions enabled
        /// </summary>
        private bool _codeSuggestionsEnabled = true;

        /// <summary>
        /// Store configured telemetry enabled
        /// </summary>
        private bool _telemetryEnabled = true;

        /// <summary>
        /// Store configured GitLab URL
        /// </summary>
        private string _gitlabUrl = string.Empty;

        private LogEventLevel _logLevel = LogEventLevel.Warning;
        private ISettings _settings;

        /// <summary>
        /// The process name of a language server instance.
        /// This is the executable name w/o the .exe.
        /// </summary>
        public static string[] LanguageServerProcessNames { get; } = {
            "gitlab-code-suggestions-language-server-windows-amd64",
            "gitlab-lsp-win-x64",
        };

        static TestData()
        {
            GitLabUrl = Environment.GetEnvironmentVariable("GITLAB_SERVER");
            if (GitLabUrl == null)
                GitLabUrl = "https://gitlab.com";

            CodeSuggestionsToken = Environment.GetEnvironmentVariable("GITLAB_TOKEN");
            if (CodeSuggestionsToken == null)
                throw new ArgumentException("Required environment variable 'GITLAB_TOKEN' was not found. This is required to run the tests.");

            SolutionPath = FindSolutionDirectory();
        }

        static string FindSolutionDirectory()
        {
            var dir = Environment.CurrentDirectory;

            while (!File.Exists(Path.Combine(dir, $"{SolutionName}.sln")))
            {
                dir = Path.GetDirectoryName(dir);
                if(dir == null)
                    throw new ApplicationException($"Expected tests to be run from a directory that is a child of the solution folder. Unable to locate '{SolutionName}.sln' in parent directories.");
            }

            return dir;
        }

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            Logging.ConfigureLogging(null);

            CreateBuilder()
                .RegisterLogging()
                .RegisterSettings()
                .RegisterObservables()
                .BuildScope();

            _settings = _scope.Resolve<ISettings>();

            if (_settings.Configured)
            {
                _gitlabUrl = _settings.GitLabUrl;
                _userAccessToken = _settings.GitLabAccessToken;
                _codeSuggestionsEnabled = _settings.IsCodeSuggestionsEnabled;
                _telemetryEnabled = _settings.IsTelemetryEnabled;
                _logLevel = _settings.LogLevel;
            }

            ResetSettings(_settings);
        }

        [OneTimeTearDown]
        public void OneTimeTeardown()
        {
            _settings.GitLabUrl = _gitlabUrl;
            _settings.GitLabAccessToken = _userAccessToken;
            _settings.IsCodeSuggestionsEnabled = _codeSuggestionsEnabled;
            _settings.IsTelemetryEnabled = _telemetryEnabled;
            _settings.LogLevel = _logLevel;
        }

        public static void ResetSettings(ISettings settings)
        {
            settings.GitLabUrl = GitLabUrl;
            settings.GitLabAccessToken = CodeSuggestionsToken;
            settings.IsCodeSuggestionsEnabled = true;
            settings.IsTelemetryEnabled = true;
            settings.LogLevel = LogEventLevel.Debug;
        }

        public static bool InCiPipeline()
        {
            return !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("CI_COMMIT_SHA"));
        }
    }
}
