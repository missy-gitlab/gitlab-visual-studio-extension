using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.CodeSuggestions.Model;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.Workspace;
using Moq;
using NUnit.Framework;
using Serilog;

namespace GitLab.Extension.Tests.CodeSuggestions
{
    [TestFixture]
    [SuppressMessage("Style", "VSTHRD200:Use \"Async\" suffix for async methods")]
    public class GitLabCodeSuggestionTelemetryControllerTests
    {
        private const string TestTelemetryId = "test-telemetry-id";
        
        private Mock<IWorkspaceLsClientProvider> _mockLsClientProvider;
        private Mock<ILsClient> _mockLsClient;
        private Mock<ILogger> _mockLogger;
        private GitLabCodeSuggestionTelemetryController _controller;

        private static readonly WorkspaceId _testWorkspaceId =
            new WorkspaceId(
                "TestSolution",
                "path/to/solution");
        
        private static readonly GitLabProposalMetadata _testProposalMetadata =
            new GitLabProposalMetadata(
                _testWorkspaceId,
                TestTelemetryId);
        
        [SetUp]
        public void SetUp()
        {
            _mockLsClientProvider = new Mock<IWorkspaceLsClientProvider>();
            _mockLsClient = new Mock<ILsClient>();
            _mockLogger = new Mock<ILogger>();
            _controller = new GitLabCodeSuggestionTelemetryController(_mockLsClientProvider.Object, _mockLogger.Object);
            
            _mockLsClientProvider.Setup(p => p.GetClient(It.IsAny<WorkspaceId>())).Returns(_mockLsClient.Object);
        }
        
        [Test]
        public async Task OnShownAsync_ShouldSendShownTelemetry()
        {
            // Act
            await _controller.OnShownAsync(_testProposalMetadata);

            // Assert
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionShownAsync(TestTelemetryId), Times.Once);
            _mockLogger.VerifyNoOtherCalls();
        }
        
        [Test]
        public async Task OnRejectedAsync_AfterOnShownAsync_ShouldSendRejectedTelemetry()
        {
            // Arrange
            await _controller.OnShownAsync(_testProposalMetadata);
            
            // Act
            await _controller.OnRejectedAsync(_testProposalMetadata);

            // Assert
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionRejectedAsync(TestTelemetryId), Times.Once);
            _mockLogger.VerifyNoOtherCalls();
        }
        
        [Test]
        public async Task OnAcceptedAsync_AfterOnShownAsync_ShouldSendAcceptedTelemetry()
        {
            // Arrange
            await _controller.OnShownAsync(_testProposalMetadata);
            
            // Act
            await _controller.OnAcceptedAsync(_testProposalMetadata);

            // Assert
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionAcceptedAsync(TestTelemetryId), Times.Once);
            _mockLogger.VerifyNoOtherCalls();
        }
        
        [Test]
        public async Task OnShownAsync_WhenExceptionOccurs_ShouldLogError()
        {
            // Arrange
            _mockLsClient.Setup(client => client.SendGitlabTelemetryCodeSuggestionShownAsync(It.IsAny<string>())).ThrowsAsync(new Exception("Test exception"));
            
            // Act
            await _controller.OnShownAsync(_testProposalMetadata);

            // Assert
            _mockLogger.Verify(logger => logger.Error(It.IsAny<Exception>(), It.IsAny<string>()), Times.Once);
        }
        
        [Test]
        public async Task OnShownAsync_WhenCalledMultipleTimes_ShouldProcessOnce()
        {
            await _controller.OnShownAsync(_testProposalMetadata);
            await _controller.OnShownAsync(_testProposalMetadata); // Second call

            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionShownAsync(TestTelemetryId), Times.Once);
            _mockLogger.Verify(logger => logger.Debug(It.IsAny<string>()), Times.Once);
        }
        
        [Test]
        public async Task OnDuplicateRejectedEvents_ForSameProposal_ShouldProcessOnlyOne()
        {
            // Arrange
            await _controller.OnShownAsync(_testProposalMetadata);
            
            // Act
            await _controller.OnRejectedAsync(_testProposalMetadata);
            await _controller.OnRejectedAsync(_testProposalMetadata);
            
            // Assert
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionRejectedAsync(TestTelemetryId), Times.Once);
        }
        
        [Test]
        public async Task OnDuplicateAcceptedEvents_ForSameProposal_ShouldProcessOnlyOne()
        {
            // Arrange
            await _controller.OnShownAsync(_testProposalMetadata);
            
            // Act
            await _controller.OnAcceptedAsync(_testProposalMetadata);
            await _controller.OnAcceptedAsync(_testProposalMetadata);
            
            // Assert
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionAcceptedAsync(TestTelemetryId), Times.Once);
        }
        
        [Test]
        public async Task OnRejectedAndAcceptedAsync_ForSameProposal_ShouldProcessOnlyOne()
        {
            // Arrange
            await _controller.OnShownAsync(_testProposalMetadata);
            
            // Act
            await _controller.OnRejectedAsync(_testProposalMetadata);
            await _controller.OnAcceptedAsync(_testProposalMetadata);
            
            // Assert
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionRejectedAsync(TestTelemetryId), Times.Once);
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionAcceptedAsync(TestTelemetryId), Times.Never);
        }
        
        [Test]
        public async Task OnShownAndAcceptedAsync_ForSameProposal_ShouldProcessBoth()
        {
            // Act
            await _controller.OnShownAsync(_testProposalMetadata);
            await _controller.OnAcceptedAsync(_testProposalMetadata);

            // Assert
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionShownAsync(TestTelemetryId), Times.Once);
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionAcceptedAsync(TestTelemetryId), Times.Once);
        }
    }
}
