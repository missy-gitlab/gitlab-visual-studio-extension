using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using GitLab.Extension.CodeSuggestions.State;
using NUnit.Framework;

namespace GitLab.Extension.Tests.CodeSuggestions.State.Enabled
{
    [TestFixture]
    public class CodeSuggestionsEnabledStateProviderTests
    {
        [Test]
        public void Constructor_WithNullValidators_InitializesWithEnabledState()
        {
            // Arrange
            var testObserver = new TestObserver<CodeSuggestionsEnabledState>();
            var validators = new List<ICodeSuggestionsEnabledValidator>(); // Empty list to simulate no validators

            // Act
            using var provider = new CodeSuggestionsEnabledStateProvider(validators);
            using var subscription = provider.CodeSuggestionEnabledStateObservable.Subscribe(testObserver);

            // Assert
            Assert.AreEqual(1, testObserver.Values.Count);
            Assert.AreEqual(CodeSuggestionsEnabledState.Enabled, testObserver.Values[0],
                "Expected initial state to be Enabled when no validators are present.");
        }

        [Test]
        public void CodeSuggestionEnabledStateObservable_WhenNotAllValidatorsHaveEmitted_EmitsInitialState()
        {
            // Arrange
            var testObserver = new TestObserver<CodeSuggestionsEnabledState>();

            using var validatorSubject =
                new Subject<CodeSuggestionsEnabledState>();

            var validators =
                new List<ICodeSuggestionsEnabledValidator>
                {
                    new TestValidator(validatorSubject)
                };

            using var provider = new CodeSuggestionsEnabledStateProvider(validators);

            // Act
            using var subscription = provider.CodeSuggestionEnabledStateObservable.Subscribe(testObserver);

            // Assert
            Assert.AreEqual(1, testObserver.Values.Count);
            Assert.AreEqual(CodeSuggestionsEnabledState.Initial, testObserver.Values[0],
                "Expected state to be set to Initial when not all validators have returned values.");
        }

        [Test]
        public void CodeSuggestionEnabledStateObservable_WhenAllValidatorsPublishSuccessResults_EmitsEnabledState()
        {
            // Arrange

            var testObserver = new TestObserver<CodeSuggestionsEnabledState>();

            using var validatorSubject1 =
                new BehaviorSubject<CodeSuggestionsEnabledState>(CodeSuggestionsEnabledState.Enabled);

            using var validatorSubject2 =
                new BehaviorSubject<CodeSuggestionsEnabledState>(CodeSuggestionsEnabledState.Enabled);

            var validators =
                new List<ICodeSuggestionsEnabledValidator>
                {
                    new TestValidator(validatorSubject1.AsObservable()),
                    new TestValidator(validatorSubject2.AsObservable())
                };

            using var provider = new CodeSuggestionsEnabledStateProvider(validators);

            // Act
            using var subscription = provider.CodeSuggestionEnabledStateObservable.Subscribe(testObserver);

            // Assert
            Assert.AreEqual(1, testObserver.Values.Count);
            Assert.AreEqual(CodeSuggestionsEnabledState.Enabled, testObserver.Values[0],
                "Expected state to be Enabled when all validators publish success results.");
        }

        [Test]
        public void CodeSuggestionEnabledStateObservable_WhenAnyValidatorPublishesDisabledState_EmitsDisabledState()
        {
            // Arrange
            var testObserver = new TestObserver<CodeSuggestionsEnabledState>();

            using var validatorSubject1 =
                new BehaviorSubject<CodeSuggestionsEnabledState>(CodeSuggestionsEnabledState.Enabled);

            // This validator publishes a state indicating code suggestions should be disabled
            using var validatorSubject2 =
                new BehaviorSubject<CodeSuggestionsEnabledState>(CodeSuggestionsEnabledState.Disabled("Test error"));

            var validators = new List<ICodeSuggestionsEnabledValidator>
            {
                new TestValidator(validatorSubject1.AsObservable()),
                new TestValidator(validatorSubject2.AsObservable())
            };

            using var provider = new CodeSuggestionsEnabledStateProvider(validators);

            // Act
            using var subscription = provider.CodeSuggestionEnabledStateObservable.Subscribe(testObserver);

            // Assert
            Assert.AreEqual(1, testObserver.Values.Count);
            Assert.IsFalse(testObserver.Values[0].IsCodeSuggestionsEnabled,
                "Code suggestions should be disabled when any validator publishes a disabled state.");
        }

        [Test]
        public void CodeSuggestionEnabledStateObservable_WhenValidatorsChangeState_ReflectsLatestState()
        {
            // Arrange
            var testObserver = new TestObserver<CodeSuggestionsEnabledState>();
            var validatorSubject = new BehaviorSubject<CodeSuggestionsEnabledState>(CodeSuggestionsEnabledState.Enabled);

            var validators = new List<ICodeSuggestionsEnabledValidator>
            {
                new TestValidator(validatorSubject.AsObservable())
            };

            using var provider = new CodeSuggestionsEnabledStateProvider(validators);

            // Act
            using var subscription = provider.CodeSuggestionEnabledStateObservable.Subscribe(testObserver);
            validatorSubject.OnNext(CodeSuggestionsEnabledState.Disabled("Change state"));

            // Assert
            Assert.AreEqual(2, testObserver.Values.Count);
            Assert.IsTrue(testObserver.Values[0].IsCodeSuggestionsEnabled, "Initial state should be enabled.");
            Assert.IsFalse(testObserver.Values[1].IsCodeSuggestionsEnabled,
                "State should be disabled after publishing a disabled state.");
        }

        private class TestValidator : ICodeSuggestionsEnabledValidator
        {
            public TestValidator(
                IObservable<CodeSuggestionsEnabledState> observable)
            {
                ValidationResultObservable = observable;
            }

            public IObservable<CodeSuggestionsEnabledState> ValidationResultObservable { get; }
        }
    }
}
