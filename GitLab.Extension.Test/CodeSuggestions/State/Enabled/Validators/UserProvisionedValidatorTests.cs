using System;
using System.Reactive.Linq;
using System.Threading;
using GitLab.Extension.CodeSuggestions.State.Validators;
using GitLab.Extension.GitLabApi;
using GitLab.Extension.SettingsUtil;
using Moq;
using NUnit.Framework;

namespace GitLab.Extension.Tests.CodeSuggestions.State.Enabled.Validators
{
    public class UserProvisionedValidatorTests
    {
        private Mock<IGitLabClientFactory> _gitLabClientFactoryMock;
        private Mock<IGitLabClient> _gitLabClientMock;
        private Mock<ISettings> _settingsMock;

        [SetUp]
        public void SetUp()
        {
            _settingsMock = new Mock<ISettings>();
            _gitLabClientFactoryMock = new Mock<IGitLabClientFactory>();
            _gitLabClientMock = new Mock<IGitLabClient>();

            _settingsMock
                .SetupGet(s => s.GitLabUrl)
                .Returns("https://gitlab.example.com");
            _settingsMock
                .SetupGet(s => s.GitLabAccessToken)
                .Returns("valid_token");

            _gitLabClientFactoryMock
                .Setup(factory => factory.CreateClient(It.IsAny<GitLabApiSettings>()))
                .Returns(_gitLabClientMock.Object);
        }

        [Test]
        public void ValidationResultObservable_UserProvisioned_EmitsEnabledState()
        {
            // Arrange
            _gitLabClientMock
                .Setup(client => client.IsCodeSuggestionsEnabledForUserAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            var settingsObservable = Observable.Return(_settingsMock.Object);
            var validator = new UserProvisionedValidator(settingsObservable, _gitLabClientFactoryMock.Object);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state => { Assert.IsTrue(state.IsCodeSuggestionsEnabled); });
        }

        [Test]
        public void ValidationResultObservable_UserNotProvisioned_EmitsDisabledState()
        {
            // Arrange
            _gitLabClientMock
                .Setup(client => client.IsCodeSuggestionsEnabledForUserAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(false);

            var settingsObservable = Observable.Return(_settingsMock.Object);
            var validator = new UserProvisionedValidator(settingsObservable, _gitLabClientFactoryMock.Object);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state =>
            {
                Assert.IsFalse(state.IsCodeSuggestionsEnabled);
                Assert.AreEqual(Constants.Messages.CodeSuggestionsNotProvisionedForUser, state.DisabledMessage);
            });
        }

        [Test]
        public void ValidationResultObservable_ErrorDuringCheck_EmitsErrorState()
        {
            // Arrange
            var expectedException = new Exception("Network failure");
            _gitLabClientMock
                .Setup(client => client.IsCodeSuggestionsEnabledForUserAsync(It.IsAny<CancellationToken>()))
                .ThrowsAsync(expectedException);

            var settingsObservable = Observable.Return(_settingsMock.Object);
            var validator = new UserProvisionedValidator(settingsObservable, _gitLabClientFactoryMock.Object);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state =>
            {
                Assert.IsFalse(state.IsCodeSuggestionsEnabled);
            });
        }
    }
}
