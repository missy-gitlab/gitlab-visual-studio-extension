using System;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using GitLab.Extension.CodeSuggestions.State;
using GitLab.Extension.CodeSuggestions.State.Validators;
using GitLab.Extension.LanguageServer.Models;
using GitLab.Extension.SettingsUtil;
using Moq;
using NUnit.Framework;

namespace GitLab.Extension.Tests.CodeSuggestions.State.Enabled.Validators
{
    [TestFixture]
    public class LsTokenCheckValidatorTests
    {
        [SetUp]
        public void SetUp()
        {
            _settingsSubject = new Subject<ISettings>();
            _tokenValidationSubject = new Subject<TokenValidationNotification>();
        }

        private Subject<ISettings> _settingsSubject;
        private Subject<TokenValidationNotification> _tokenValidationSubject;

        [Test]
        public void ValidationResultObservable_InitialCondition_EmitsEnabledState()
        {
            // Arrange
            var validator =
                new LsTokenCheckValidator(_settingsSubject.AsObservable(), _tokenValidationSubject.AsObservable());

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state => { Assert.IsTrue(state.IsCodeSuggestionsEnabled); });
        }

        [Test]
        public void ValidationResultObservable_InvalidToken_EmitsDisabledState()
        {
            // Arrange
            var validator =
                new LsTokenCheckValidator(_settingsSubject.AsObservable(), _tokenValidationSubject.AsObservable());

            // Act
            _tokenValidationSubject.OnNext(new TokenValidationNotification());

            // Assert
            validator.ValidationResultObservable.Subscribe(state =>
            {
                Assert.IsFalse(state.IsCodeSuggestionsEnabled);
                Assert.AreEqual(Extension.CodeSuggestions.State.Constants.Messages.InvalidAccessToken,
                    state.DisabledMessage);
            });
        }

        [Test]
        public void ValidationResultObservable_SettingsChangeAfterInvalidToken_EmitsCorrectState()
        {
            // Arrange
            var testObserver = new TestObserver<CodeSuggestionsEnabledState>();
            var validator =
                new LsTokenCheckValidator(_settingsSubject.AsObservable(), _tokenValidationSubject.AsObservable());
            using var subscription = validator.ValidationResultObservable.Subscribe(testObserver);

            // Act & Assert

            Assert.IsTrue(testObserver.Values.Last()
                .IsCodeSuggestionsEnabled);

            _tokenValidationSubject.OnNext(new TokenValidationNotification());

            Assert.IsFalse(testObserver.Values.Last()
                .IsCodeSuggestionsEnabled);

            _settingsSubject.OnNext(Mock.Of<ISettings>());

            Assert.IsTrue(testObserver.Values.Last()
                .IsCodeSuggestionsEnabled);
        }
    }
}
