using System;
using System.Reactive.Linq;
using GitLab.Extension.CodeSuggestions.State.Validators;
using GitLab.Extension.SettingsUtil;
using Moq;
using NUnit.Framework;

namespace GitLab.Extension.Tests.CodeSuggestions.State.Enabled.Validators
{
    [TestFixture]
    public class SettingsConfiguredValidatorTests
    {
        [SetUp]
        public void SetUp()
        {
            _settingsMock = new Mock<ISettings>();
        }

        private Mock<ISettings> _settingsMock;

        [Test]
        public void ValidationResultObservable_SettingsConfigured_EmitsEnabledState()
        {
            // Arrange
            _settingsMock
                .Setup(s => s.Configured)
                .Returns(true);

            var settingsObservable = Observable.Return(_settingsMock.Object);
            var validator = new SettingsConfiguredValidator(settingsObservable);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state => { Assert.IsTrue(state.IsCodeSuggestionsEnabled); });
        }

        [Test]
        public void ValidationResultObservable_SettingsNotConfigured_EmitsDisabledState()
        {
            // Arrange
            _settingsMock
                .Setup(s => s.Configured)
                .Returns(false);

            var settingsObservable = Observable.Return(_settingsMock.Object);
            var validator = new SettingsConfiguredValidator(settingsObservable);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state =>
            {
                Assert.IsFalse(state.IsCodeSuggestionsEnabled);
                Assert.AreEqual(
                    Extension.CodeSuggestions.State.Constants.Messages.ExtensionNotConfigured,
                    state.DisabledMessage);
            });
        }
    }
}
