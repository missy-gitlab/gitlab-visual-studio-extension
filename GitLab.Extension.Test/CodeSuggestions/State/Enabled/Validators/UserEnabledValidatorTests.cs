using System;
using System.Reactive.Linq;
using GitLab.Extension.CodeSuggestions.State.Validators;
using GitLab.Extension.SettingsUtil;
using Moq;
using NUnit.Framework;

namespace GitLab.Extension.Tests.CodeSuggestions.State.Enabled.Validators
{
    [TestFixture]
    public class UserEnabledValidatorTests
    {
        [SetUp]
        public void SetUp()
        {
            _settingsMock = new Mock<ISettings>();
        }

        private Mock<ISettings> _settingsMock;

        [Test]
        public void ValidationResultObservable_SettingsCodeSuggestionsEnabled_EmitsEnabledState()
        {
            // Arrange
            _settingsMock
                .Setup(s => s.IsCodeSuggestionsEnabled)
                .Returns(true);

            var settingsObservable = Observable.Return(_settingsMock.Object);
            var validator = new UserEnabledValidator(settingsObservable);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state => { Assert.IsTrue(state.IsCodeSuggestionsEnabled); });
        }

        [Test]
        public void ValidationResultObservable_SettingsCodeSuggestionsDisabled_EmitsDisabledState()
        {
            // Arrange
            _settingsMock
                .Setup(s => s.IsCodeSuggestionsEnabled)
                .Returns(false);

            var settingsObservable = Observable.Return(_settingsMock.Object);
            var validator = new UserEnabledValidator(settingsObservable);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state =>
            {
                Assert.IsFalse(state.IsCodeSuggestionsEnabled);
                Assert.AreEqual(
                    null,
                    state.DisabledMessage);
            });
        }
    }
}
