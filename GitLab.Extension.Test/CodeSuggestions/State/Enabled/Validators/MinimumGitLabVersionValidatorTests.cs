using System;
using System.Reactive.Linq;
using System.Threading;
using GitLab.Extension.CodeSuggestions.State.Validators;
using GitLab.Extension.GitLabApi;
using GitLab.Extension.GitLabApi.Model;
using GitLab.Extension.SettingsUtil;
using Moq;
using NUnit.Framework;

namespace GitLab.Extension.Tests.CodeSuggestions.State.Enabled.Validators
{
    public class MinimumGitLabVersionValidatorTests
    {
        private Mock<IGitLabClientFactory> _gitLabClientFactoryMock;
        private Mock<IGitLabClient> _gitLabClientMock;
        private Mock<ISettings> _settingsMock;

        [SetUp]
        public void SetUp()
        {
            _settingsMock = new Mock<ISettings>();
            _gitLabClientFactoryMock = new Mock<IGitLabClientFactory>();
            _gitLabClientMock = new Mock<IGitLabClient>();

            _settingsMock
                .SetupGet(s => s.GitLabUrl)
                .Returns("https://gitlab.com");
            _settingsMock
                .SetupGet(s => s.GitLabAccessToken)
                .Returns("valid_token");

            _gitLabClientFactoryMock
                .Setup(f => f.CreateClient(It.IsAny<GitLabApiSettings>()))
                .Returns(_gitLabClientMock.Object);
        }

        [Test]
        public void ValidationResultObservable_SupportedGitLabVersion_EmitsEnabledState()
        {
            // Arrange
            var metadata = new Metadata { Version = "16.8.0" };
            _gitLabClientMock
                .Setup(client => client.GetMetadataAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(metadata);

            var settingsObservable = Observable.Return(_settingsMock.Object);
            var validator = new MinimumGitLabVersionValidator(settingsObservable, _gitLabClientFactoryMock.Object);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state => { Assert.IsTrue(state.IsCodeSuggestionsEnabled); });
        }

        [Test]
        public void ValidationResultObservable_UnsupportedGitLabVersion_EmitsDisabledState()
        {
            // Arrange
            var metadata = new Metadata { Version = "16.7.0" };
            _gitLabClientMock
                .Setup(client => client.GetMetadataAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(metadata);

            var settingsObservable = Observable.Return(_settingsMock.Object);
            var validator = new MinimumGitLabVersionValidator(settingsObservable, _gitLabClientFactoryMock.Object);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state =>
            {
                Assert.IsFalse(state.IsCodeSuggestionsEnabled);
            });
        }
    }
}
