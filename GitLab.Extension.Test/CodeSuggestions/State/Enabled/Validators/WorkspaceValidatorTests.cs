using System;
using System.Reactive.Linq;
using System.Threading;
using GitLab.Extension.CodeSuggestions.State.Validators;
using GitLab.Extension.GitLabApi;
using GitLab.Extension.GitLabApi.Model;
using GitLab.Extension.SettingsUtil;
using GitLab.Extension.Utility.Git;
using GitLab.Extension.Utility.Results;
using GitLab.Extension.Utility.Results.Errors;
using GitLab.Extension.Workspace;
using LibGit2Sharp;
using Moq;
using NUnit.Framework;

namespace GitLab.Extension.Tests.CodeSuggestions.State.Enabled.Validators
{
    [TestFixture]
    public class WorkspaceValidatorTests
    {
        [SetUp]
        public void SetUp()
        {
            _settingsMock = new Mock<ISettings>();
            _workspaceStateObservableMock = new Mock<IActiveWorkspaceStateObservable>();
            _clientFactoryMock = new Mock<IGitLabClientFactory>();
            _repositoryFactoryMock = new Mock<IRepositoryFactory>();
            _workspaceMock = new Mock<IWorkspace>();
            _clientMock = new Mock<IGitLabClient>();
            _repositoryMock = new Mock<IRepository>();

            // Setup mocks with default behaviors
            _settingsMock
                .SetupGet(s => s.GitLabUrl)
                .Returns("https://gitlab.example.com");
            _settingsMock
                .SetupGet(s => s.GitLabAccessToken)
                .Returns("valid_token");
            _clientFactoryMock
                .Setup(f => f.CreateClient(It.IsAny<GitLabApiSettings>()))
                .Returns(_clientMock.Object);
            _repositoryFactoryMock
                .Setup(f => f.GetRepository(It.IsAny<string>()))
                .Returns(Result<IRepository>.Ok(_repositoryMock.Object));
        }

        private Mock<ISettings> _settingsMock;
        private Mock<IActiveWorkspaceStateObservable> _workspaceStateObservableMock;
        private Mock<IGitLabClientFactory> _clientFactoryMock;
        private Mock<IRepositoryFactory> _repositoryFactoryMock;
        private Mock<IWorkspace> _workspaceMock;
        private Mock<IGitLabClient> _clientMock;
        private Mock<IRepository> _repositoryMock;

        [Test]
        public void ValidationResultObservable_NoActiveWorkspace_EmitsDisabledState()
        {
            // Arrange
            var settingsObservable = Observable.Return(_settingsMock.Object);
            var workspaceStateObservable = Observable.Return<WorkspaceState>(WorkspaceState.NoActiveWorkspace);

            _workspaceStateObservableMock
                .As<IObservable<WorkspaceState>>()
                .Setup(x => x.Subscribe(It.IsAny<IObserver<WorkspaceState>>()))
                .Returns(workspaceStateObservable.Subscribe);

            var validator =
                new WorkspaceValidator(
                    settingsObservable,
                    _workspaceStateObservableMock.Object,
                    _clientFactoryMock.Object,
                    _repositoryFactoryMock.Object);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state =>
            {
                Assert.IsFalse(state.IsCodeSuggestionsEnabled);
                Assert.AreEqual(Extension.CodeSuggestions.State.Constants.Messages.NoActiveWorkspace,
                    state.DisabledMessage);
            });
        }

        [Test]
        public void ValidationResultObservable_WorkspaceInitializing_EmitsDisabledState()
        {
            // Arrange
            var workspaceId = new WorkspaceId("SolutionName", "path/to/solution");
            var settingsObservable = Observable.Return(_settingsMock.Object);
            var workspaceStateObservable =
                Observable.Return<WorkspaceState>(WorkspaceState.WorkspaceInitializing(workspaceId));

            _workspaceStateObservableMock
                .As<IObservable<WorkspaceState>>()
                .Setup(x => x.Subscribe(It.IsAny<IObserver<WorkspaceState>>()))
                .Returns(workspaceStateObservable.Subscribe);

            var validator =
                new WorkspaceValidator(
                    settingsObservable,
                    _workspaceStateObservableMock.Object,
                    _clientFactoryMock.Object,
                    _repositoryFactoryMock.Object);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state =>
            {
                Assert.IsFalse(state.IsCodeSuggestionsEnabled);
                Assert.AreEqual(Extension.CodeSuggestions.State.Constants.Messages.WorkspaceInitializing,
                    state.DisabledMessage);
            });
        }

        [Test]
        public void ValidationResultObservable_WorkspaceInitializationFailed_EmitsDisabledState()
        {
            // Arrange
            var workspaceId = new WorkspaceId("SolutionName", "path/to/solution");

            var settingsObservable = Observable.Return(_settingsMock.Object);
            var workspaceStateObservable =
                Observable.Return<WorkspaceState>(
                    WorkspaceState.WorkspaceInitializationFailed(
                        workspaceId,
                        new Error("An error occured")));

            _workspaceStateObservableMock
                .As<IObservable<WorkspaceState>>()
                .Setup(x => x.Subscribe(It.IsAny<IObserver<WorkspaceState>>()))
                .Returns(workspaceStateObservable.Subscribe);

            var validator = new WorkspaceValidator(settingsObservable, _workspaceStateObservableMock.Object,
                _clientFactoryMock.Object, _repositoryFactoryMock.Object);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state =>
            {
                Assert.IsFalse(state.IsCodeSuggestionsEnabled);
                Assert.AreEqual(
                    Extension.CodeSuggestions.State.Constants.Messages.WorkspaceInitializationFailed,
                    state.DisabledMessage);
            });
        }

        [Test]
        public void ValidationResultObservable_WorkspaceActiveAndDuoEnabled_EmitsEnabledState()
        {
            // Arrange
            var settingsObservable = Observable.Return(_settingsMock.Object);
            var workspaceStateObservable =
                Observable.Return<WorkspaceState>(new WorkspaceActive(_workspaceMock.Object));

            _workspaceStateObservableMock
                .As<IObservable<WorkspaceState>>()
                .Setup(x => x.Subscribe(It.IsAny<IObserver<WorkspaceState>>()))
                .Returns(workspaceStateObservable.Subscribe);

            _clientMock
                .Setup(client =>
                    client.IsCodeSuggestionsEnabledForProjectAsync(It.IsAny<CodeSuggestionsEnabledRequest>(),
                        It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            var validator = new WorkspaceValidator(settingsObservable, _workspaceStateObservableMock.Object,
                _clientFactoryMock.Object, _repositoryFactoryMock.Object);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state => { Assert.IsTrue(state.IsCodeSuggestionsEnabled); });
        }

        [Test]
        public void ValidationResultObservable_WorkspaceActiveButDuoDisabled_EmitsDisabledState()
        {
            // Arrange
            var settingsObservable = Observable.Return(_settingsMock.Object);
            var workspaceStateObservable =
                Observable.Return<WorkspaceState>(new WorkspaceActive(_workspaceMock.Object));

            _workspaceStateObservableMock
                .As<IObservable<WorkspaceState>>()
                .Setup(x => x.Subscribe(It.IsAny<IObserver<WorkspaceState>>()))
                .Returns(workspaceStateObservable.Subscribe);

            _clientMock
                .Setup(client =>
                    client.IsCodeSuggestionsEnabledForProjectAsync(It.IsAny<CodeSuggestionsEnabledRequest>(),
                        It.IsAny<CancellationToken>()))
                .ReturnsAsync(false);

            var validator = new WorkspaceValidator(settingsObservable, _workspaceStateObservableMock.Object,
                _clientFactoryMock.Object, _repositoryFactoryMock.Object);

            // Act & Assert
            validator.ValidationResultObservable.Subscribe(state =>
            {
                Assert.IsFalse(state.IsCodeSuggestionsEnabled);
                Assert.AreEqual(
                    Extension.CodeSuggestions.State.Constants.Messages.CodeSuggestionDisabledForProject,
                    state.DisabledMessage);
            });
        }
    }
}
