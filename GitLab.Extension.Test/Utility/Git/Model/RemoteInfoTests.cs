﻿using GitLab.Extension.Utility.Git.Model;
using NUnit.Framework;

namespace GitLab.Extension.Tests.Utility.Git.Model
{
    [TestFixture]
    public class RemoteInfoTests
    {
        [TestCase("https://example.com/my-namespace/my-project.git", "my-namespace", new string[0], "my-project")]
        [TestCase("ssh://example.com/my-namespace/my-project.git", "my-namespace", new string[0], "my-project")]
        [TestCase("https://example.com/my-namespace/my-project", "my-namespace", new string[0], "my-project")]
        [TestCase("https://example.com/my-namespace/my-group/my-project", "my-namespace", new[] { "my-group" }, "my-project")]
        [TestCase("ssh://example.com/my-namespace/group/subgroup/project", "my-namespace", new[] { "group", "subgroup" }, "project")]
        public void ParseRemoteUrl_ValidUrls_ReturnsExpectedRemoteInfo(
            string url, 
            string expectedNamespace, 
            string[] expectedGroups, 
            string expectedProjectPath)
        {
            // Act
            var result = RemoteInfo.ParseRemoteUrl(url);

            // Assert
            Assert.IsTrue(result.IsSuccess(out var remoteInfo));
            Assert.AreEqual(expectedNamespace, remoteInfo.Namespace);
            CollectionAssert.AreEqual(expectedGroups, remoteInfo.Groups);
            Assert.AreEqual(expectedProjectPath, remoteInfo.Project);
        }
        
        [TestCase("not-a-valid-url")]
        [TestCase(null)]
        [TestCase("https://example.com/")]
        public void ParseRemoteUrl_InvalidUrl_ReturnsError(
            string url)
        {
            // Act
            var result = RemoteInfo.ParseRemoteUrl(url);

            // Assert
            Assert.IsTrue(result.IsError(out _));
        }
    }
}