﻿using System.Threading.Tasks;
using GitLab.Extension.Utility.Results;
using NUnit.Framework;
using static GitLab.Extension.Utility.Results.Result;

namespace GitLab.Extension.Tests.Utility
{
    [TestFixture]
    public class ResultTests
    {
        [SetUp]
        public void SetUp()
        {
            _ok = Ok<int>(InitialOkValue);
            _err = Error<int>(InitialErrorValue);
        }

        private const int InitialOkValue = 42;
        private const string InitialErrorValue = "Error!";

        private Result<int> _ok;
        private Result<int> _err;

        [Test]
        public void Factory_Create_SuccessResult_ReturnsSuccess()
        {
            Assert.True(_ok.IsSuccess(out var okIntValue) && okIntValue == InitialOkValue);
            Assert.False(_ok.IsError(out _));
        }

        [Test]
        public void Factory_Create_ErrorResult_ReturnsError()
        {
            Assert.True(_err.IsError(out var errStrErr) && errStrErr.Message == InitialErrorValue);
            Assert.False(_err.IsSuccess(out _));
        }

        [Test]
        public void Match_OnSuccess_ReturnsMappedValue()
        {
            var okResult = _ok.Match(
                x => x * 2,
                _ => -1);

            Assert.AreEqual(84, okResult);
        }

        [Test]
        public void Match_OnError_ReturnsValueFromFailurePath()
        {
            var errResult = _err.Match(
                x => x * 2,
                _ => -1);

            Assert.AreEqual(-1, errResult);
        }

        [Test]
        public void Match_WithSideEffects_UpdatesVariableOutsideClosure()
        {
            var output = 0;

            void OnSuccess(int x) => output += x;
            void OnFailure(IError e) => output -= e.Message == InitialErrorValue ? 1 : 0;

            _ok.Match(OnSuccess, OnFailure);
            Assert.AreEqual(InitialOkValue, output);

            _err.Match(OnSuccess, OnFailure);
            Assert.AreEqual(InitialOkValue - 1, output);
        }

        [Test]
        public void Map_CanMapValues()
        {
            // Arrange
            decimal MapFunc(
                int x) => (decimal)x * 2;

            // Act
            var mappedOk = _ok.Map(MapFunc);
            var mappedErr = _err.Map(MapFunc);

            // Assert
            Assert.True(mappedOk.IsSuccess(out var value) && value == 84);
            Assert.True(mappedErr.IsError(out var error) && error.Message == InitialErrorValue);
        }

        [Test]
        public async Task MapAsync_CanMapValuesAsync()
        {
            // Arrange
            Task<decimal> MapFunc(int x) =>
                Task.FromResult((decimal)x * 2);

            // Act
            var mappedOk = await _ok.Map(MapFunc);
            var mappedErr = await _err.Map(MapFunc);

            // Assert
            Assert.True(mappedOk.IsSuccess(out var value) && value == 84);
            Assert.True(mappedErr.IsError(out var error) && error.Message == InitialErrorValue);
        }

        [Test]
        public async Task BindAsync_WithValidValue_ReturnsMappedResultAsync()
        {
            async Task<Result<string>> BindFuncAsync(int value) =>
                await Task.FromResult(Ok($"Value: {value}"));

            var result = await _ok.Bind(BindFuncAsync);

            Assert.IsTrue(result.IsSuccess(out var rValue) && rValue == "Value: 42");
        }

        [Test]
        public void Bind_WithValidValue_ReturnsMappedResult()
        {
            Result<string> BindFunc(int value) =>
                Ok($"Value: {value}");

            var result = _ok.Bind(BindFunc);

            Assert.IsTrue(result.IsSuccess(out var rValue) && rValue == "Value: 42");
        }

        [Test]
        public void Bind_WithInvalidValue_ReturnsInitialErrorResult()
        {
            Result<string> BindFunc(int value) =>
                Error<string>("Invalid value");

            var result = _err.Bind(BindFunc);

            Assert.IsTrue(result.IsError(out var error) && error.Message == "Error!");
        }
    }
}