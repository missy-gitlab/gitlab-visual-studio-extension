﻿using GitLab.Extension.LanguageServer;
using GitLab.Extension.SettingsUtil;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Autofac;

namespace GitLab.Extension.Tests.LanguageServer
{
    [TestFixture]
    public class LsProcessManagerTsTests : TestBase
    {
        [SetUp]
        public void Setup()
        {
            CreateBuilder()
                .RegisterLogging()
                .RegisterCodeSuggestions()
                .RegisterSettings()
                .RegisterStatus()
                .RegisterLanguageServer(LsImpl.TypeScript)
                .BuildScope();

            LsCommon.KillLanguageServer();
        }

        [Test]
        public async Task StartAndStopAsync()
        {
            Assert.IsFalse(
                LsCommon.IsLanguageServerRunning(out _), 
                "Expected that no language servers would exist before test is run");

            var ret = _scope.Resolve<ILsProcessManager>().StartLanguageServerStdio(
                TestData.SolutionPath, TestData.GitLabUrl, TestData.CodeSuggestionsToken, 
                out var lsInput,
                out var lsOutput);

            Assert.IsTrue(ret, "Expected StartLanguageServer to return true.");
            Assert.IsTrue(
                LsCommon.IsLanguageServerRunning(out var count), 
                "Expected language server to have been started.");
            Assert.AreEqual(1, count, $"Expected 1 process, but got {count} processes.");

            try
            {
                ret = _scope.Resolve<ILsProcessManager>().StartLanguageServerStdio(
                    TestData.SolutionPath, TestData.GitLabUrl, TestData.CodeSuggestionsToken,
                    out var lsInputSecond,
                    out var lsOutputSecond);

                Assert.IsFalse(ret, "Expected second StartLanguageServer call to return false.");
                Assert.IsTrue(lsInput == lsInputSecond, $"Expected lsInput and lsInputSecond to be the same.");
                LsCommon.IsLanguageServerRunning(out count);
                Assert.AreEqual(1, count, $"Expected 1 process after second StartLanguageServer, but got {count} processes.");
            }
            finally
            {
                ret = await _scope.Resolve<ILsProcessManager>().StopLanguageServerAsync(TestData.SolutionPath);

                Assert.IsTrue(ret, "Expected StopLanguageServerAsync to return true.");
                Assert.IsFalse(
                    LsCommon.IsLanguageServerRunning(TimeSpan.FromSeconds(10), out _),
                    "Expected the language server process to have been stopped.");
            }

            // Make sure starting it again works as expected

            try
            {
                ret = _scope.Resolve<ILsProcessManager>().StartLanguageServerStdio(
                    TestData.SolutionPath, TestData.GitLabUrl, TestData.CodeSuggestionsToken,
                    out var lsInputThird,
                    out var lsOutputThird);

                Assert.IsTrue(ret, "Expected StartLanguageServer to return true.");
                Assert.IsTrue(
                    LsCommon.IsLanguageServerRunning(out count),
                    "Expected language server to have been started.");
                Assert.AreEqual(1, count, $"Expected 1 process, but got {count} processes.");
                Assert.IsFalse(lsInput == lsInputThird,
                    $"Expected lsInput and lsInputThird to be different."); ;

                ret = _scope.Resolve<ILsProcessManager>().StartLanguageServerStdio(
                    TestData.SolutionPath, TestData.GitLabUrl, TestData.CodeSuggestionsToken,
                    out var lsInputForth,
                    out var lsOutputForth);

                Assert.IsFalse(ret, "Expected second StartLanguageServer call to return false.");
                Assert.IsTrue(lsInputThird == lsInputForth, $"Expected lsInputThird and lsInputForth to be the same.");
                LsCommon.IsLanguageServerRunning(out count);
                Assert.AreEqual(1, count, $"Expected 1 process after second StartLanguageServer, but got {count} processes.");
            }
            finally
            {
                ret = await _scope.Resolve<ILsProcessManager>().StopLanguageServerAsync(TestData.SolutionPath);
                Assert.IsTrue(ret, "Expected StopLanguageServerAsync to return true.");
                Assert.IsFalse(
                    LsCommon.IsLanguageServerRunning(TimeSpan.FromSeconds(10), out _),
                    "Expected the language server process to have been stopped.");
            }
        }
    }
}
