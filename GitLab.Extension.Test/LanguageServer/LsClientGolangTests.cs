﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.SettingsUtil;
using System.Threading;
using System.IO;
using System.Net;
using Autofac;
using Serilog;

namespace GitLab.Extension.Tests.LanguageServer
{
    [TestFixture]
    [Ignore("Dericated")]
    public class LsClientGolangTests : TestBase
    {
        private ISettings _settings;

        [SetUp]
        public void Setup()
        {
            LsCommon.KillLanguageServer();

            Assert.IsFalse(LsCommon.IsLanguageServerRunning(out var _), 
                "Expected no language servers to be running at start of test (Setup)");

            CreateBuilder()
                .RegisterLogging()
                .RegisterCodeSuggestions()
                .RegisterSettings()
                .RegisterStatus()
                .RegisterLanguageServer(LsImpl.Golang)
                .BuildScope();

            _settings = _scope.Resolve<ISettings>();
            TestData.ResetSettings(_settings);
        }

        [TearDown]
        public void Teardown()
        {
            TestData.ResetSettings(_settings);

            try
            {
                Assert.IsFalse(LsCommon.IsLanguageServerRunning(TimeSpan.FromSeconds(10), out var _),
                    "Expected no language servers to be running at end of test (Teardown)");
            }
            finally
            {
                _scope.Dispose();
                _scope = null;
            }
        }

        public async Task<ILsClient> StartLsClientAsync()
        {
            var lsClient = _scope.Resolve<ILsClient>(
                new TypedParameter(typeof(LsClientSolution),
                new LsClientSolution(TestData.SolutionName, TestData.SolutionPath)));
            var ret = await lsClient.ConnectAsync();
            Assert.IsTrue(ret, "Expected LsClient to connect");

            return lsClient;
        }

        public static string GetTempFilePathWithExtension(string extension)
        {
            var path = Path.GetTempPath();
            string fileName;
            string tmpFile;

            do
            {
                fileName = Path.ChangeExtension(Guid.NewGuid().ToString(), extension);
                tmpFile = Path.Combine(path, fileName);
            }
            while (File.Exists(tmpFile));

            return tmpFile;
        }

        public static string GetFileUrl(string fileName)
        {
            return $"file://{fileName.Replace('\\', '/')}";
        }

        [Test]
        public async Task ConnectTestAsync()
        {
            using (var lsClient = _scope.Resolve<ILsClient>(
                new TypedParameter(typeof(LsClientSolution),
                new LsClientSolution(TestData.SolutionName, TestData.SolutionPath))))
            {
                var ret = await lsClient.ConnectAsync();
                Assert.IsTrue(ret, "Expected LsClient to connect");
            }
        }

        [Test]
        public async Task DisposeWithOutConnectTestAsync()
        {
            var lsClient = _scope.Resolve<ILsClient>(
                new TypedParameter(typeof(LsClientSolution),
                new LsClientSolution(TestData.SolutionName, TestData.SolutionPath)));
            await lsClient.DisposeAsync();

            lsClient = _scope.Resolve<ILsClient>(
                new TypedParameter(typeof(LsClientSolution),
                new LsClientSolution(TestData.SolutionName, TestData.SolutionPath)));
            lsClient.Dispose();
        }

        [Test]
        public async Task ReconnectionTestAsync()
        {
            Log.Debug("*** ReconnectionTestAsync ***");

            using (var lsClient = await StartLsClientAsync())
            {
                Log.Debug("*** ReconnectionTestAsync: LsCommon.VerifyLsClientWorkingAsync #1");
                await LsCommon.VerifyLsClientWorkingAsync(lsClient);

                Log.Debug("*** ReconnectionTestAsync: LsCommon.KillLsClientAndWaitForReconnect");
                LsCommon.KillLsClientAndWaitForReconnect(lsClient);

                Log.Debug("*** ReconnectionTestAsync: LsCommon.KillLsClientAndWaitForReconnect #2");
                await LsCommon.VerifyLsClientWorkingAsync(lsClient);
            }
        }

        [Test]
        public async Task TextDocumentDidOpenAsync()
        {
            using (var lsClient = await StartLsClientAsync())
            {
                Assert.IsTrue(await lsClient.SendTextDocumentDidOpenAsync(
                        "file:///foo.cs",
                        0,
                        "namespace Bar { public class Foo { } }"),
                    "Expected SendTextDocumentDidOpenAsync to return true");
            }
        }

        [Test]
        public async Task TextDocumentDidChangeAsync()
        {
            using (var lsClient = await StartLsClientAsync())
            {
                Assert.IsTrue(await lsClient.SendTextDocumentDidOpenAsync(
                        "file:///foo.cs",
                        0,
                        "namespace Bar { public class Foo { } }"),
                    "Expected SendTextDocumentDidOpenAsync to return true");

                Assert.IsTrue(await lsClient.SendTextDocumentDidChangeAsync(
                        "file:///foo.cs",
                        1,
                        "namespace Bar { public class Foo { public Foo() { } } }"
                        ),
                    "Expected SendTextDocumentDidChangeAsync to return true");
            }
        }

        [Test]
        public async Task TextDocumentCompletionAsync()
        {
            var tmpFile = GetTempFilePathWithExtension(".cs");
            var codeV0 = "namespace Bar {\n\tpublic class Foo {\n\t}\n}";
            var codeV1 = "namespace Bar {\n\tpublic class Foo {\n\t\tpublic Foo() {\n\t\t\t\n\t\t}\n\t}\n}";
            uint codeV1Line = 3;
            uint codeV1Pos = 3;
            try
            {
                File.WriteAllText(tmpFile, "namespace Bar {\n\tpublic class Foo {\n\t\tpublic Foo() {\n\t\t}\n\t}\n}");

                using (var lsClient = await StartLsClientAsync())
                {
                    bool ret;
                    var tokenSource = new CancellationTokenSource();

                    ret = await lsClient.SendTextDocumentDidOpenAsync(
                        tmpFile,
                        0,
                        codeV0);
                    Assert.IsTrue(ret, "Expect SendTextDocumentDidOpenAsync to return true");

                    ret = await lsClient.SendTextDocumentDidChangeAsync(
                        tmpFile,
                        1,
                        codeV1);
                    Assert.IsTrue(ret, "Expect SendTextDocumentDidChangeAsync to return true");

                    // Make sure the didChange gets processed before asking for completion
                    await Task.Delay(250);

                    var (completions, error) = await lsClient.SendTextDocumentCompletionAsync(
                        tmpFile,
                        codeV1Line,
                        codeV1Pos,
                        tokenSource.Token);

                    Assert.IsNotNull(completions, "Expected a non-null result from SendTextDocumentCompletionAsync");
                    Assert.GreaterOrEqual(completions.Length, 1, "Expected at least one completion.");
                    Assert.NotZero(completions[0].insertText.Length, "Expected completion inserText to be longer than zero.");
                }
            }
            finally
            {
                if(File.Exists(tmpFile))
                    File.Delete(tmpFile);
            }
        }

        [Test]
        public async Task RestartLsProcessWhenSettingsChangeAsync()
        {
            var initialAccessToken = _settings.GitLabAccessToken;

            try
            {
                using (var lsClient = await StartLsClientAsync())
                {
                    var initialProcessId = LsCommon.GetLanguageServerProcessId();

                    _settings.GitLabAccessToken = "glpat-abcdef";

                    // Allow upto 5 seconds for the LS to be restarted

                    var afterProcessId = -1;
                    var startTime = DateTime.Now;
                    var waitTime = TimeSpan.FromSeconds(5);
                    do
                    {
                        afterProcessId = LsCommon.GetLanguageServerProcessId();
                    }
                    while((initialProcessId == afterProcessId || afterProcessId == -1) &&
                        (DateTime.Now - startTime < waitTime));

                    Assert.AreNotEqual(-1, afterProcessId, "Expected a language server process to be found after settings change");
                    Assert.AreNotEqual(initialProcessId, afterProcessId, "Expected the LS to have been restarted and have a different process ID");
                }
            }
            finally
            {
                _settings.GitLabAccessToken = initialAccessToken;
            }
        }

        private string _userAgent;

        private void UserAgentTestHandler(HttpListenerRequest request, HttpListenerResponse response)
        {
            _userAgent = request.Headers["user-agent"];

            response.StatusCode = 200;
            response.StatusDescription = "OK";
            response.ContentLength64 = 0;
        }

        [Test]
        public async Task UserAgentTestAsync()
        {
            _userAgent = null;

            using (var listener = new TestHttpListener(UserAgentTestHandler))
            {
                var tmpFile = GetTempFilePathWithExtension(".cs");
                var codeV0 = "namespace Bar {\n\tpublic class Foo {\n\t}\n}";
                var codeV1 = "namespace Bar {\n\tpublic class Foo {\n\t\tpublic Foo() {\n\t\t\t\n\t\t}\n\t}\n}";
                uint codeV1Line = 3;
                uint codeV1Pos = 3;

                try
                {
                    File.WriteAllText(tmpFile, "namespace Bar {\n\tpublic class Foo {\n\t\tpublic Foo() {\n\t\t}\n\t}\n}");

                    _settings.GitLabUrl = listener.Url;

                    using (var lsClient = await StartLsClientAsync())
                    {
                        bool ret;
                        var tokenSource = new CancellationTokenSource();

                        ret = await lsClient.SendTextDocumentDidOpenAsync(
                            tmpFile,
                            0,
                            codeV0);
                        Assert.IsTrue(ret, "Expect SendTextDocumentDidOpenAsync to return true");

                        ret = await lsClient.SendTextDocumentDidChangeAsync(
                            tmpFile,
                            1,
                            codeV1);
                        Assert.IsTrue(ret, "Expect SendTextDocumentDidChangeAsync to return true");

                        // Make sure the didChange gets processed before asking for completion
                        await Task.Delay(250);

                        var (completions, error) = await lsClient.SendTextDocumentCompletionAsync(
                            tmpFile,
                            codeV1Line,
                            codeV1Pos,
                            tokenSource.Token);

                        // Occationally the LS process will hang/not shutdown
                        // if the listener isn't closed first.
                        listener.Dispose();
                    }
                }
                catch(Exception ex)
                {
                    if (ex.Message.Contains("unexpected 200 OK response when acquiring token"))
                        return;

                    throw;
                }
                finally
                {
                    if (File.Exists(tmpFile))
                        File.Delete(tmpFile);
                }

                Assert.IsNotNull(_userAgent, "Expected a non-null user-agent");
                Assert.IsTrue(_userAgent.Contains("(gl-visual-studio-extension:"),
                    $"Expected the user-agent to contain '(gl-visual-studio-extension:' but instead got '{_userAgent}'");
            }
        }
    }
}
