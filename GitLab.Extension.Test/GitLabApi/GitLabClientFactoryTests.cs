﻿using System;
using System.Net.Http;
using GitLab.Extension.GitLabApi;
using GitLab.Extension.Utility.Results;
using Moq;
using NUnit.Framework;

namespace GitLab.Extension.Tests.GitLabApi
{
    [TestFixture]
    public class GitLabClientFactoryTests
    {
        private Mock<IHttpClientFactory> _mockHttpClientFactory;
        private GitLabClientFactory _gitLabClientFactory;

        [SetUp]
        public void SetUp()
        {
            _mockHttpClientFactory = new Mock<IHttpClientFactory>();
            _gitLabClientFactory = new GitLabClientFactory(_mockHttpClientFactory.Object);
        }

        [Test]
        public void CreateClient_ConfiguresHttpClientWithCorrectSettings_ReturnsGitLabClient()
        {
            // Arrange
            var fakeHttpClient = new HttpClient();
            var settings = GitLabApiSettings.Create(
                baseUrl: "https://example.gitlab.com/",
                accessToken: "test_access_token"
            ).GetValueOrDefault();

            _mockHttpClientFactory
                .Setup(f => f.CreateClient(Extension.GitLabApi.Constants.HttpClientName))
                .Returns(fakeHttpClient);

            // Act
            var result = _gitLabClientFactory.CreateClient(settings);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf<GitLabClient>(result);
            Assert.AreEqual(settings.BaseUri, fakeHttpClient.BaseAddress);
            Assert.AreEqual($"bearer {settings.AccessToken}",
                fakeHttpClient.DefaultRequestHeaders.Authorization.ToString());

            _mockHttpClientFactory.Verify(
                f => f.CreateClient(Extension.GitLabApi.Constants.HttpClientName),
                Times.Once);
        }
    }
}