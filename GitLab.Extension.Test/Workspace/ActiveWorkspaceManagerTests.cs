using System;
using System.Linq;
using System.Reactive.Subjects;
using GitLab.Extension.Utility.Results;
using GitLab.Extension.Utility.Results.Errors;
using GitLab.Extension.Workspace;
using GitLab.Extension.Workspace.Model;
using Moq;
using NUnit.Framework;

namespace GitLab.Extension.Tests.Workspace
{
    public class ActiveWorkspaceManagerTests
    {
        private BehaviorSubject<WorkspaceId?> _activeWorkspaceIdSubject;
        private Mock<IWorkspaceFactory> _mockWorkspaceFactory;
        private ActiveWorkspaceManager _activeWorkspaceManager;
        private TestObserver<WorkspaceState> _observer;

        private static WorkspaceId _testWorkspaceId =
            new WorkspaceId(
                "TestSolution",
                "path/to/solution");
        
        [SetUp]
        public void SetUp()
        {
            _activeWorkspaceIdSubject = new BehaviorSubject<WorkspaceId?>(null);
            
            var mockActiveWorkspaceIdObservable = 
                new Mock<IActiveWorkspaceIdObservable>();

            mockActiveWorkspaceIdObservable
                .Setup(x => x.Subscribe(It.IsAny<IObserver<WorkspaceId?>>()))
                .Returns((IObserver<WorkspaceId?> observer) => _activeWorkspaceIdSubject.Subscribe(observer));
            
            _mockWorkspaceFactory = new Mock<IWorkspaceFactory>();
            _activeWorkspaceManager = new ActiveWorkspaceManager(mockActiveWorkspaceIdObservable.Object, _mockWorkspaceFactory.Object);
            _observer = new TestObserver<WorkspaceState>();
        }
        
        [TearDown]
        public void TearDown()
        {
            _activeWorkspaceManager.Dispose();
        }
        
        [Test]
        public void Subscribe_Initial_EmitNoActiveWorkspace()
        {
            // Act
            using (_activeWorkspaceManager.Subscribe(_observer))
            {}
            
            // Assert
            Assert.That(_observer.Values.First(), Is.InstanceOf<NoActiveWorkspace>());
        }
        
        [Test]
        public void OnWorkspaceOpen_InitializesAndActivatesWorkspaceSuccess_EmitsWorkspaceActive()
        {
            // Arrange
            _mockWorkspaceFactory.Setup(f => f.CreateWorkspaceAsync(_testWorkspaceId))
                .ReturnsAsync(Result<IWorkspace>.Ok(new WorkspaceModel(_testWorkspaceId)));

            // Act
            using (_activeWorkspaceManager.Subscribe(_observer))
            {
                _activeWorkspaceIdSubject.OnNext(_testWorkspaceId);
            }

            // Assert
            Assert.That(_observer.Values.Count, Is.EqualTo(3));
            Assert.That(_observer.Values[0], Is.InstanceOf<NoActiveWorkspace>());
            Assert.That(_observer.Values[1], Is.InstanceOf<WorkspaceInitializing>());
            Assert.That(_observer.Values[2], Is.InstanceOf<WorkspaceActive>());
        }
        
        [Test]
        public void OnWorkspaceOpen_FailedInitialization_EmitsInitializationFailed()
        {
            // Arrange
            _mockWorkspaceFactory.Setup(f => f.CreateWorkspaceAsync(_testWorkspaceId))
                .ReturnsAsync(Result<IWorkspace>.Error(new Error("Something went wrong")));

            // Act
            using (_activeWorkspaceManager.Subscribe(_observer))
            {
                _activeWorkspaceIdSubject.OnNext(_testWorkspaceId);
            }

            // Assert
            Assert.That(_observer.Values.Count, Is.EqualTo(3));
            Assert.That(_observer.Values[0], Is.InstanceOf<NoActiveWorkspace>());
            Assert.That(_observer.Values[1], Is.InstanceOf<WorkspaceInitializing>());
            Assert.That(_observer.Values[2], Is.InstanceOf<WorkspaceInitializationFailed>());
        }
        
        [Test]
        public void OnWorkspaceClose_ClosesActiveWorkspace_EmitsNoActiveWorkspace()
        {
            // Arrange
            _mockWorkspaceFactory.Setup(f => f.CreateWorkspaceAsync(_testWorkspaceId))
                .ReturnsAsync(Result<IWorkspace>.Ok(new WorkspaceModel(_testWorkspaceId)));

            // Act
            using (_activeWorkspaceManager.Subscribe(_observer))
            {
                _activeWorkspaceIdSubject.OnNext(_testWorkspaceId);
                _activeWorkspaceIdSubject.OnNext(null);
            }

            // Assert
            Assert.That(_observer.Values.Count, Is.EqualTo(4));
            Assert.That(_observer.Values[0], Is.InstanceOf<NoActiveWorkspace>());
            Assert.That(_observer.Values[1], Is.InstanceOf<WorkspaceInitializing>());
            Assert.That(_observer.Values[2], Is.InstanceOf<WorkspaceActive>());
            Assert.That(_observer.Values[3], Is.InstanceOf<NoActiveWorkspace>());
        }
        
        [Test]
        public void MultipleSubscribers_ReceiveUpdatesCorrectly()
        {
            // Arrange
            var observer2 = new TestObserver<WorkspaceState>();
            _mockWorkspaceFactory.Setup(f => f.CreateWorkspaceAsync(It.IsAny<WorkspaceId>()))
                .ReturnsAsync(Result<IWorkspace>.Ok(new WorkspaceModel(_testWorkspaceId)));

            // Act
            using (_activeWorkspaceManager.Subscribe(_observer))
            using (_activeWorkspaceManager.Subscribe(observer2))
            {
                _activeWorkspaceIdSubject.OnNext(_testWorkspaceId);
            }

            // Assert
            Assert.That(_observer.Values, Is.EquivalentTo(observer2.Values));
        }
        
        [Test]
        public void OnWorkspaceOpen_WhenErrorOccurs_EmitsInitializationFailed()
        {
            // Arrange
            _mockWorkspaceFactory.Setup(f => f.CreateWorkspaceAsync(_testWorkspaceId))
                .ThrowsAsync(new Exception("Simulated failure"));

            // Act
            using (_activeWorkspaceManager.Subscribe(_observer))
            {
                _activeWorkspaceIdSubject.OnNext(_testWorkspaceId);
            }

            // Assert
            Assert.That(_observer.Values.Last(), Is.InstanceOf<WorkspaceInitializationFailed>());
        }
        
        [Test]
        public void OnWorkspaceOpen_SameWorkspaceOpenedTwice_EmitsOnce()
        {
            // Arrange
            _mockWorkspaceFactory.Setup(f => f.CreateWorkspaceAsync(_testWorkspaceId))
                .ReturnsAsync(Result<IWorkspace>.Ok(new WorkspaceModel(_testWorkspaceId)));

            // Act
            using (_activeWorkspaceManager.Subscribe(_observer))
            {
                _activeWorkspaceIdSubject.OnNext(_testWorkspaceId);
                _activeWorkspaceIdSubject.OnNext(_testWorkspaceId);
            }
            
            // Assert
            Assert.That(_observer.Values.Count, Is.EqualTo(3)); // NoActiveWorkspace, Initializing, Active
        }
    }
}
