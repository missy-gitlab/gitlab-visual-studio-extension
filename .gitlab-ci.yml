include:
  template: Security/Dependency-Scanning.gitlab-ci.yml
variables:
  DS_MAX_DEPTH: 6
  APP_PATH: 'C:\GitLab-Runner\builds\gitlab-org\editor-extensions\gitlab-visual-studio-extension\'
  VSIX_RELEASE_FOLDER: '.\GitLab.Extension\bin\Release'
  MSI_RELEASE_FOLDER: 'Setup\bin\Release'
  TEST_FOLDER: 'GitLab.Extension.Test\bin\Debug\net472'
  DEPLOY_FOLDER: 'P:\Projects\GitLab.Extension\Builds'
  NUGET_PATH: 'C:\NuGet\nuget.exe'
  MSBUILD_PATH: 'C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\MSBuild\Current\Bin\MSBuild.exe'
  NUNIT_PATH: 'C:\Program Files (x86)\NUnit.org\nunit-console\nunit3-console.exe'

.shared_windows_runners:
  tags:
    - shared-windows
    - windows
    - windows-1809

stages:
  - documentation
  - pre-build
  - build
  - test
  - release
  - publish

gemnasium-dependency_scanning:
  needs: []

check_docs_markdown:
  stage: documentation
  needs: []
  image: registry.gitlab.com/gitlab-org/gitlab-docs/lint-markdown:alpine-3.19-vale-3.0.7-markdownlint-0.39.0-markdownlint2-0.12.1
  script:
    # Lint prose
    - vale --minAlertLevel error docs README.md CONTRIBUTING.md CHANGELOG.md
    # Lint Markdown
    - markdownlint-cli2 'docs/**/*.md' *.md

pre-build:
  stage: pre-build
  script:
    - apt-get update -y
    - apt-get install -y --no-install-recommends git-lfs jq
    - echo "This job will only run on main."
    - echo "This will be a linux runner and we will"
    - echo "- generate the buildtag"
    - ./CICD/jobs/pre-build
  artifacts:
    # save gitlab server space, we copy the files we need to deploy folder later on
    expire_in: 1 week
    paths:
      - output/*

build:
  extends:
    - .shared_windows_runners
  needs: ['pre-build']
  stage: build
  script:
    - Write-Output "start of script, getting ready to run powershell scripts"
    - '.\CICD\update_manifest.ps1'
    - '.\CICD\update_assemblyinfo.ps1'
    - '.\CICD\base_install.ps1'
    - '.\CICD\build_sln.ps1'
  artifacts:
    # save gitlab server space, we copy the files we need to deploy folder later on
    expire_in: 1 week
    paths:
      - '.\GitLab.Extension.Test\bin\Debug\net472\\**'
      - '.\GitLab.Extension\source.extension.vsixmanifest'
      - '.\GitLab.Extension\Properties\AssemblyInfo.cs'
      - '.\GitLab.Extension\bin\Release\GitLab.Extension.vsix'

test:
  extends:
    - .shared_windows_runners
  stage: test
  needs: ['build']
  script:
    - '& CICD\jobs\test.bat'
  artifacts:
    # save test results even when the task fails
    when: always
    # save gitlab server space, we copy the files we need to deploy folder later on
    expire_in: 1 week
    # saving NUnit results to copy to deploy folder
    paths:
      - TestResult.xml
      - junit-results.xml
      - coverage_report.xml
    # ingest reports
    reports:
      junit:
        - junit-results.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage_report.xml

release:
  stage: release
  needs: ['build']
  when: manual
  allow_failure: false
  only:
    - main
  script:
    - echo "This is a manual job."
    - echo "This job will only run on main."
    - echo "This will be a linux runner and we will"
    - echo "- add the changelog"
    - echo "- commit the changes to the changelog and the vsixmanifest with [ci skip]"
    - echo "- create the tag"
    - echo "- create the release"
    - apt-get update -y
    - apt-get install -y --no-install-recommends git-lfs jq
    - ./CICD/jobs/release

publish:
  extends:
    - .shared_windows_runners
  stage: publish
  needs: ['release', 'build', 'pre-build']
  allow_failure: false
  only:
    - main
  script:
    - Write-Output "start of script, getting ready to run powershell scripts and then publish the VSIX"
    - '.\CICD\base_install.ps1'
    # - '.\CICD\build_sln.ps1'
    - '.\CICD\publish_to_marketplace.ps1'
